########################################################################################
#                                                                                      #
#   Author: Julien Rey,                                                                #
#   Organization:'MTi' Laboratory, Universite Paris Diderot, Paris.                    #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

import logging 
_log = logging.getLogger(__name__)

from SlurmDRMAAConfig import SlurmDRMAAConfig
         
class SlurmAcctDRMAAConfig( SlurmDRMAAConfig ):
    
        def __init__( self , drmaa_library_path , nativeSpecification = '', qos = '' ):
            super( SlurmAcctDRMAAConfig , self ).__init__( drmaa_library_path , nativeSpecification = nativeSpecification, qos = qos )

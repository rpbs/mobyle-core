// run an alignment on the current alignFrame
import jalview.ws.jws2.*;
import jalview.datamodel.*;

// First get the JABA service discoverer and see if there are any services.
jalview.ws.jws2.Jws2Discoverer disc = jalview.ws.jws2.Jws2Discoverer.getDiscoverer();
while (disc.isRunning()) {
Thread.sleep(50);
}
if (!disc.isRunning() && !disc.hasServices())
{
  // discoverer is not running, and has no services - so run it again.
  disc.run();
}

for (jalview.ws.jws2.Jws2Discoverer.Jws2Instance service: disc.getServices()) {

if (service.serviceType.indexOf("uscle")>-1) {
  // now - go through the services if any, and find a Muscle service
  def msaf;
  try {
	msaf = currentAlFrame;
  } catch (q) {
        // currentAlFrame is not defined - so we were run as an interactive script from the Groovy console
	// in that case, just pick the first alignmentFrame in the stack.
	msaf = Jalview.getAlignframes()[0]
  };
  // Finally start Jalview's JabaWS MSA Client with the alignment from msaf
  new MsaWSClient(service, msaf.getTitle(), msaf.gatherSequencesForAlignment(), false,
                true, msaf.getViewport().getAlignment()
                .getDataset(), msaf);
  break;
}
}

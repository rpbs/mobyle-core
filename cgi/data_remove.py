#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi

def process( self ):
    if self.session:
        bookmark_ids = self.request.getlist('id')
        for data_id in bookmark_ids:
            if(self.session.hasData(data_id)):
                self.session.removeData(data_id)
    self.jsonMap['ok'] = str(True)

if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)
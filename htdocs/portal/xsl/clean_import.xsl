<?xml version="1.0" encoding="utf-8"?>
<!-- 
	ident.xsl stylesheet
	Authors: Hervé Ménager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:xhtml="http://www.w3.org/1999/xhtml">
	<!-- This stylesheet contains the XSL Identity Transform used by many other stylesheets -->

	<xsl:include href="ident.xsl" />
	
	<xsl:template match="/">
		<xsl:apply-templates select="@*|node()|text()" />
	</xsl:template>

	<xsl:template match="interface[@generated='true']" />

</xsl:stylesheet>
#####################################################
# Here are the different email templates sent to the user
# Each template is structured as follows:
#
# From:         required
# Cc:           optional  
# Subject:      required
# Reply-To:     optional
#               an empty line
# the body of the email containing some keys to expand
#
###################################################



############################################################
#
# CONFIRM_SESSION email is sent to the user to confirm his 
# registration. 
# if config.AUTHENTICATED_SESSION = 'email'
# 
# the available keys are:
# SENDER      : the email address which sends the mail as defined in the configuration
# HELP        : the email of the Mobyle "hot line" as defined in the configuration
# SERVER_NAME : the url of the Mobyle portal
# CGI_URL     : the url of the portal cgi directory
# ACTIVATING_KEY : the key which allows the user to activate its session 
###########################################################

CONFIRM_SESSION = """
From: %(SENDER)s
Cc: 
Bcc: 
Subject: [ Mobyle server ( %(SERVER_NAME)s ) ] - new user confirmation" 
Reply-To: %(SENDER)s
Organization: Institut Pasteur, Paris.

You have requested an account on the %(SERVER_NAME)s Mobyle server.
Your login is: your email
Your activation key is: %(ACTIVATING_KEY)s 
To activate this account please click on the
following link (or paste its URL in your favourite browser):
%(CGI_URL)s/portal.py#user::activate
"""

####################################################################
#
# RESULTS_TOOBIG email is sent to the user when a job is finished 
# and the size of the results files is greater than Config.MAXMAILSIZE
# 
# the available keys are:
# SENDER      : the email address which sends the mail as defined in the configuration 
# HELP        : the email of the Mobyle "hot line" as defined in the configuration
# SERVER_NAME : the url of the Mobyle portal
# JOB_URL     : the url of the job
# JOB_NAME    : the name of the service ex blast2 , clustalw-multialign
# RESULTS_REMAIN : the number of days before the job is removed from the server, as defined in the configuration
#
####################################################################

RESULTS_TOOBIG = """
From: %(SENDER)s
Reply-To: %(SENDER)s
Subject: Your %(JOB_NAME)s job is finished. Access your results

Your results are too big to be sent by email.
They are accessible at the following address:
%(JOB_URL)s

Your results will be kept on our server for %(RESULTS_REMAIN)d days from now.
"""


####################################################################
#
# RESULTS_FILES email is sent to the user when a job is finished 
# Config.OPT_EMAIl = False
# 
# the available keys are:
# SENDER : the email address which sends the mail as defined in the configuration 
# HELP   : the email of the mobyle "hot line" as defined in the configuration
# SERVER_NAME : the url of the mobyle portal
# JOB_URL     : the url of the job
# JOB_NAME    : the name of the service ex blast2 , clustalw-multialign
# RESULTS_REMAIN : the number of days before the jobs will be erase as defined in the configuration
#
####################################################################

RESULTS_FILES = """
From: %(HELP)s
Reply-To: %(SENDER)s
Subject: Your %(JOB_NAME)s job is finished. Access your results

Your results are accessible at the following address:
%(JOB_URL)s

Your results will be kept on our server for %(RESULTS_REMAIN)d days from now.

An archive of your job is attached to this email. 
It includes the index.xml file which is a summary of your job submission 
(you can view it in your favorite web browser).
""" 

####################################################################
#
# RESULTS_NOTIFICATION email is sent to the user when a job is finished 
# but the results files zipping failed.
# 
# the available keys are:
# SENDER : the email address which sends the mail as defined in the configuration 
# HELP   : the email of the mobyle "hot line" as defined in the configuration
# SERVER_NAME : the url of the mobyle portal
# JOB_URL     : the url of the job
# JOB_NAME    : the name of the service ex blast2 , clustalw-multialign
# RESULTS_REMAIN : the number of days before the jobs will be erase as defined in the configuration
#
####################################################################

RESULTS_NOTIFICATION = """
From: %(HELP)s
Reply-To: %(SENDER)s
Subject: Your %(JOB_NAME)s job is finished. Access your results.

Your results are accessible at the following address:
%(JOB_URL)s

Your results will be kept on our server for %(RESULTS_REMAIN)d days from now.
"""

####################################################################
#
# LONG_JOB_NOTIFICATION email is sent to the user when a job is longer
# than Config.TIMEOUT
# 
# the available keys are:
# SENDER   : the email address which sends the mail as defined in the configuration 
# HELP     : the email of the mobyle "hot line" as defined in the configuration
# SERVER_NAME : the url of the mobyle portal
# JOB_URL  : the url of the job
# JOB_NAME : the name of the service ex blast2 , clustalw-multialign
# JOB_KEY  : the unique key wich permit to identify a job
# RESULTS_REMAIN : the number of days before the jobs will be erase as defined in the configuration
#
####################################################################

LONG_JOB_NOTIFICATION = """
From: %(HELP)s
Reply-To: %(SENDER)s
Subject: Your job %(JOB_NAME)s/%(JOB_KEY)s is running.

Your %(JOB_URL)s job is running on %(SERVER_NAME)s server.
You will receive the results by email.
You can access the results or check the job status at the following address:
%(JOB_URL)s
"""

####################################################################
#
#  email is send From the user to the mobyle help adress to request some help 
# 
# the available keys are:
# USER     : the user email address
# SENDER   : the email address which sends the mail as defined in the configuration 
# HELP     : the email of the mobyle "hot line" as defined in the configuration
# MSG      : the message written by the USER 
# SERVER_NAME   : the url of the mobyle portal
# SESSION_ID    : the session unique identifier
# SESSION_EMAIL : the user email store in the session
# SESSION_ACTIVATED : if the session is activated
# SESSION_AUTHENTICATED : if the session is authenticated
# JOB_URL         : the url of the job
# JOB_DATE        : the date at which this job was launch
# JOB_STATUS      : the mobyle job status ( finished, error ... )
# JOB_ERROR_PARAM : if there is an error, the parameter name that provoke the error
# JOB_ERROR_MSG   : the error message associated with the error
####################################################################

HELP_REQUEST = """
From: %(USER)s
Subject: [mobyle help request] help on job %(JOB_URL)s.

User Message:
%(MSG)s
    
from: %(USER)s

Session information:
- id: %(SESSION_ID)s
- email: %(SESSION_EMAIL)s
- activated?: %(SESSION_ACTIVATED)s
- authenticated?: %(SESSION_AUTHENTICATED)s

Job information:
- id: %(JOB_URL)s
- date: %(JOB_DATE)s
- status: %(JOB_STATUS)s
- user error in parameter: %(JOB_ERROR_PARAM)s
- user error message: %(JOB_ERROR_MSG)s
"""

####################################################################
#
#  email is sent to the user as a receipt of its help demand
# 
# the available keys are:
# USER     : the user email address
# SENDER   : the email address which sends the mail as defined in the configuration 
# HELP     : the email of the mobyle "hot line" as defined in the configuration
# MSG      : the message written by the USER 
# SERVER_NAME   : the url of the mobyle portal
# SESSION_ID    : the session unique identifier
# SESSION_EMAIL : the user email store in the session
# SESSION_ACTIVATED : if the session is activated
# SESSION_AUTHENTICATED : if the session is authenticated
# JOB_URL         : the url of the job
# JOB_DATE        : the date at which this job was launch
# JOB_STATUS      : the mobyle job status ( finished, error ... )
# JOB_ERROR_PARAM : if there is an error, the parameter name that provoke the error
# JOB_ERROR_MSG   : the error message associated with the error
#
####################################################################
HELP_REQUEST_RECEIPT = """
From: %(HELP)s
Reply-To: %(SENDER)s
Subject: [mobyle help request receipt] help on job %(JOB_URL)s.

We have received your help request (the message is included below). 
We will try to answer it as soon as possible.

User Message:
%(MSG)s
    
from: %(USER)s

Session information:
- id: %(SESSION_ID)s
- email: %(SESSION_EMAIL)s
- activated?: %(SESSION_ACTIVATED)s
- authenticated?: %(SESSION_AUTHENTICATED)s

Job information:
- id: %(JOB_URL)s
- date: %(JOB_DATE)s
- status: %(JOB_STATUS)s
- error parameter: %(JOB_ERROR_PARAM)s
- error message: %(JOB_ERROR_MSG)s
"""

######################################################################
#
# email is sent by mobpasswd ( if -m option ) to notify the user of a 
# password modification. 
#
# HELP          : the email of the mobyle "hot line" as defined in the configuration
# SENDER        : the email address which sends the mail as defined in the configuration 
# SERVER_NAME   : the url of the mobyle portal
# PASSWD        : the new password
#
######################################################################

NEW_PASSWD = """
From: %(HELP)s
Reply-To: %(SENDER)s
Subject: [ %(SERVER_NAME)s ] new password.

You have requested to change the password for your account on the
%(SERVER_NAME)s server.

Your new password is %(PASSWD)s
"""

<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
	<!-- this stylesheet transforms a mobyle workflow from the mobyle XML format to a GRAPHML-based format -->
	<xsl:output method="xml" omit-xml-declaration="yes" indent="yes" />
	<xsl:key name="fromTaskIds" match="link" use="@fromTask" />
	<xsl:key name="toTaskIds" match="link" use="@toTask" />
	
	<xsl:template match="workflow">
		<graphml>
			<key id="name" attr.name="name" for="node" attr.type="string" />
			<key id="pause" attr.name="pause" for="node" attr.type="boolean" />
			<key id="xy" attr.name="position" for="node" attr.type="string" />
			<key id="type" attr.name="portType" for="port" attr.type="string" />
			<key id="status" attr.name="portStatus" for="port" attr.type="string" />
			<key id="processJobStatus" attr.name="jobStatus" for="node" attr.type="string"/>
			<key id="workflowJobStatus" attr.name="jobStatus" for="graph" attr.type="string"/>
			<key id="title" attr.name="workflowTitle" for="graph" attr.type="string" />
			<key id="description" attr.name="workflowDescription" for="graph" attr.type="string" />
			<graph>
					<xsl:attribute name="id">
    	                <xsl:value-of select="@id" />
	                </xsl:attribute>
					<xsl:apply-templates select="head" />
					<xsl:for-each select="flow/task">
						<node>
							<xsl:variable name="serviceDoc" select="document(@service_url)" />							
							<xsl:attribute name="id">
                                <xsl:value-of select="@id" />
                            </xsl:attribute>
							<data key="name">
								<xsl:value-of select="@service" />
							</data>
							<data key="pause">
								<xsl:choose>
									<xsl:when test="@suspend='1' or @suspend='true'">true</xsl:when>
									<xsl:otherwise>false</xsl:otherwise>
								</xsl:choose>
							</data>
							<xsl:if test="position">
								<data key="xy"><xsl:value-of select="position/text()" /></data>
							</xsl:if>
							<xsl:variable name="task_id" select="@id" />
							<xsl:for-each select="$serviceDoc//parameter">
								<xsl:variable name ="dataType">
									<xsl:choose>
										<xsl:when test="type/datatype/superclass"><xsl:value-of select="type/datatype/superclass"/></xsl:when>
										<xsl:otherwise><xsl:value-of select="type/datatype/class"/></xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:if test="not(($dataType = 'Integer')
												or ($dataType = 'Float')
												or ($dataType = 'Boolean')
												or ($dataType = 'Choice')
												or ($dataType = 'MultipleChoice')
												or ($dataType = 'Text')
												or ($dataType = 'Binary')
												or ($dataType = 'String')
												or ($dataType = 'Filename')
												or ($dataType = 'Binary'))">
									<port>
										<xsl:attribute name="name">
											<xsl:value-of select="name/text()" />
										</xsl:attribute>
										<xsl:choose>
											<xsl:when test="@isout or @isstdout">
												<data key="type">pipeOut</data>												
											</xsl:when>
											<xsl:otherwise>
												<data key="type">pipeIn</data>
											</xsl:otherwise>
										</xsl:choose>
										<data key="status">active</data>
									</port>									
								</xsl:if>
							</xsl:for-each>
							<!--
							<xsl:for-each select="../link[@fromTask=$task_id]">
								<xsl:if test="generate-id() = generate-id(key('fromTaskIds',@fromTask))">
									<port>
										<xsl:attribute name="name">
                                <xsl:value-of select="@fromParameter" />
                            </xsl:attribute>
										<data key="type">pipeOut</data>
										<data key="status">active</data>
									</port>
								</xsl:if>
							</xsl:for-each>
							<xsl:for-each select="../link[@toTask=$task_id]">
								<xsl:if test="generate-id() = generate-id(key('toTaskIds',@toTask))">

									<port>
										<xsl:attribute name="name">
                                <xsl:value-of select="@toParameter" />
                            </xsl:attribute>
										<data key="type">pipeIn</data>
										<data key="status">active</data>
									</port>
								</xsl:if>

							</xsl:for-each>
							-->
						</node>
					</xsl:for-each>
					<xsl:for-each select="flow/link">
						<edge>
							<xsl:attribute name="source">
                            <xsl:value-of select="@fromTask" />
                        </xsl:attribute>
							<xsl:attribute name="target">
                            <xsl:value-of select="@toTask" />
                        </xsl:attribute>
							<xsl:attribute name="sourcePort">
                            <xsl:value-of select="@fromParameter" />
                        </xsl:attribute>
							<xsl:attribute name="targetPort">
                            <xsl:value-of select="@toParameter" />
                             </xsl:attribute>
						</edge>
					</xsl:for-each>

				</graph>
		</graphml>
	</xsl:template>
	
	<xsl:template match="head">
		<xsl:apply-templates select="doc/title" />
		<xsl:apply-templates select="doc/description" />
		<xsl:apply-templates select="doc/comment" />
	</xsl:template>
	
	<xsl:template match="title" >
		<!-- workflow title value -->
		<data key="title"><xsl:value-of select="text()"></xsl:value-of></data>
	</xsl:template>

	<xsl:template match="description" >
		<!-- workflow description value -->
		<data key="description">
			<xsl:apply-templates select="text" />
			<xsl:apply-templates select="xhtml:*" />
		</data>
	</xsl:template>	

	<xsl:template match="comment" >
		<!-- workflow description value -->
		<data key="comment">
			<xsl:apply-templates select="text" />
			<xsl:apply-templates select="xhtml:*" />
		</data>
	</xsl:template>

	<xsl:template match="text" >
		<xsl:value-of select="text()" />
	</xsl:template>

	<xsl:template match="xhtml:*" >
		<xsl:copy-of select="." />
	</xsl:template>	

</xsl:stylesheet>


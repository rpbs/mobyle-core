#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.Registry import registry

def process(self):
    self.xslParams = {}
    try:
        def f(x): return x and x != '.'
        id = filter(f,self.request.getfirst('id').partition('.'))
        try:
            if len(id)==1:
                self.service = registry.serversByName['local'].programsByName[id[0]]    
            else:
                self.service = registry.serversByName[id[0]].programsByName[id[1]]
        except KeyError:
            if len(id)==1:
                self.service = registry.serversByName['local'].workflowsByName[id[0]]            
            else:
                self.service = registry.serversByName[id[0]].workflowsByName[id[1]]            
        if not(self.service.server.name=='local'):
            self.xslParams['remoteServerName']="'"+self.service.server.name+"'"
    except Exception:
        # program does not exist
        self.error_msg = "Service %s cannot be found on this server." % self.request.getfirst('id','')
        self.error_title = "Service not found"
        return
    if self.service.disabled:
        # program disabled
        self.error_msg = "this service has been disabled on this server"
        self.error_title = "Service disabled"
    elif not(self.service.authorized):
        # program restricted
        self.error_msg = "the access to this service is restricted on this server"
        self.error_title = "Service unauthorized"
    else:
        # ok
        self.xmlUrl = self.service.path
    self.xslPipe = [
                    (self.cfg.portal_path()+"/xsl/form.xsl",
                        {'programUri':"'"+self.service.url+"'",
                         'programPID':"'"+self.service.pid+"'"}),
                    (self.cfg.portal_path()+"/xsl/remove_ns.xsl",{}) # remove xhtml namespace junk
                    ]
    if self.service.server.name=='local':
        self.title = self.service.name
    else:
        self.title = self.request.getfirst('id')
        tmptitle = self.title.split('.')
        tmptitle.reverse()
        self.title = '@'.join(tmptitle)

if __name__ == "__main__":
    il = mb_cgi.XSLCGI(processFunction=process,useSession=True)
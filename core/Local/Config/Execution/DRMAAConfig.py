########################################################################################
#                                                                                      #
#   Author: Bertrand Neron,                                                            #
#   Organization:'Biological Software and Databases' Group, Institut Pasteur, Paris.   #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

import logging 
_log = logging.getLogger(__name__)

from ExecutionConfig import ExecutionConfig

class DRMAAConfig( ExecutionConfig ):
    
    def __init__( self, drmaa_library_path , nativeSpecification = '' ):
        super( DRMAAConfig , self ).__init__()
        self.drmaa_library_path = drmaa_library_path
        self.nativeSpecification = nativeSpecification
        self.contactString = '' 

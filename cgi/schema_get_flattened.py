#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import os
import mb_cgi
from Mobyle.Registry import registry

def process(self):
    self.mime_type = "text/xml"
    self.xslParams = {}
    self.xmlUrl = os.path.join(mb_cgi.MOBYLEHOME,'Schema',self.request.getfirst('type')+'.rng')
    self.xslPipe = [
                    (self.cfg.portal_path()+"/xsl/schema_flatten.xsl",{}) # remove xhtml namespace junk
                    ]

if __name__ == "__main__":
    il = mb_cgi.XSLCGI(processFunction=process,useSession=True)
#!/usr/bin/perl

use strict;

print "Content-type: text/html\n\n";
print <<HEADER;
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Mobyle usage statistics</title>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript">
HEADER

for my $year (2009..2018) {

	my %tab_country;
	my %tab_prog;
	my %tab_prog_monthly;

	my $file_country = "../htdocs/portal/help/statistics/stats_country_$year.txt";

	open(STATS_COUNTRY, $file_country);

	my $total;

	while(<STATS_COUNTRY>) {

		if (/^\s*(.+)\s:\s(\d+)/) {
			my $country = $1;
			my $number = $2;
			if ($number < 100) {
				$tab_country{others} += $2;
			} else {
				$tab_country{$1} = $2;
			}
			$total += $2;
		}

	}

	foreach my $country (keys %tab_country) {

		$tab_country{$country} /= $total;

	}

	close(STATS_COUNTRY);
	
	my $file_prog = "../htdocs/portal/help/statistics/stats_prog_$year.txt";
	
	open(STATS_PROG, $file_prog);

	while(<STATS_PROG>) {

		if (/^(\S+)\s+:\s+(\d+) jobs\s+([0-9.]+)%/) {
			my $prog = $1;
			my $jobs = $2;
			my $percent = $3;
			if ($percent < 0.5) {
				$tab_prog{others} += $jobs;
			} else {
				$tab_prog{$1} = $jobs;
			}
		}

	}

	close(STATS_PROG);

	for my $month (qw(01 02 03 04 05 06 07 08 09 10 11 12)) {

		my $file_prog = "../htdocs/portal/help/statistics/stats_prog_".$year."_".$month.".txt";

		open(STATS_PROG, $file_prog);

		while(<STATS_PROG>) {

			if (/^(\S+)\s+:\s+(\d+) jobs\s+([0-9.]+)%/) {
				my $prog = $1;
				my $jobs = $2;
				my $percent = $3;
				if ($percent < 0.5) {
					$tab_prog_monthly{others}{$month} += $jobs;
				} else {
					$tab_prog_monthly{$1}{$month} = $jobs;
				}
			}

		}

		close(STATS_PROG);

	}

	print "
	var chart$year;
	\$(document).ready(function() {
		chart$year = new Highcharts.Chart({
			chart: {
				renderTo: 'container$year',
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			title: {
				text: 'Mobyle statistics by country $year'
			},
			tooltip: {
				formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage*100)/100 +' %';
				}
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						color: '#000000',
						connectorColor: '#000000',
						formatter: function() {
							return this.y > 0.01 ? '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage*100)/100 +' %'  : null;
						}
					}
				}
			},
			series: [{
				type: 'pie',
				name: 'Services',
	";

	print "\t\t\tdata: [";

	my $flag;

	foreach my $country (sort { $tab_country{$b} <=> $tab_country{$a} } keys %tab_country) {
		print "\t\t\t\t['$country', $tab_country{$country}],\n" unless $country =~ "others";
		$flag++;
	}

	print "\t\t\t\t['Others', $tab_country{others}]\n";

	print "
				]
			}]
		});

	});
	
	\$(document).ready(function() {
	chart2$year = new Highcharts.Chart({
		chart: {
			renderTo: 'container2$year',
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false
			},
			title: {
				text: 'Mobyle statistics by service $year'
			},
			tooltip: {
				formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage*100)/100 +' %';
				}
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						color: '#000000',
						connectorColor: '#000000',
						formatter: function() {
							return this.y > 1 ? '<b>'+ this.point.name +'</b>: '+ this.point.y +' qry'  : null;
						}
					}
				}
			},
			series: [{
				type: 'pie',
				name: 'Services',
	";

	print "\t\t\tdata: [";

	my $flag;

	foreach my $prog (sort { $tab_prog{$b} <=> $tab_prog{$a} } keys %tab_prog) {
		print "\t\t\t\t['$prog', $tab_prog{$prog}],\n" unless $prog =~ "others";
		$flag++;
	}

	print "\t\t\t\t['Others', $tab_prog{others}]\n";

	print "
				]
			}]
		});

	});

	\$(document).ready(function() {
		chart3$year = new Highcharts.Chart({
			chart: {
				renderTo: 'container3$year',
				type: 'column'
			},
			title: {
				text: 'Mobyle statistics by service $year'
			},
			xAxis: {
				categories: [
					'Jan',
					'Feb',
					'Mar',
					'Apr',
					'May',
					'Jun',
					'Jul',
					'Aug',
					'Sep',
					'Oct',
					'Nov',
					'Dec'
				]
			},
			yAxis: {
				min:0
			},
			plotOptions: {
				column: {
					stacking: 'normal',
					dataLabels: {
						enabled: false
					}
				}
			},
			series: [";

	my $flag;

	foreach my $prog (sort keys %tab_prog_monthly) {
		print "{\n\t\t\t\t\tname:'$prog',\n\t\t\t\t\tdata: [";
		foreach my $month (qw(01 02 03 04 05 06 07 08 09 10 11 12)) {
			print ($tab_prog_monthly{$prog}{$month} ? "$tab_prog_monthly{$prog}{$month}," : "0,") unless $prog =~ "others";
			$flag++;
		}
		print "]\n\t\t\t\t},";
	}

	print "
			]
		});

	});

	"

}

print <<END;
		</script>
	</head>
	<body>
		<script type="text/javascript" src="/portal/applets/Highcharts/js/highcharts.js"></script>
		<script type="text/javascript" src="/portal/applets/Highcharts/js/modules/exporting.js"></script>
		<table border="1">
END

for my $year (2009..2018) {

	print "
		<tr>
			<td><div id='container$year' style='width: 600px; height: 400px; margin: 0 auto'></div></td>
			<td><div id='container2$year' style='width: 600px; height: 400px; margin: 0 auto'></div></td>
			<td><div id='container3$year' style='width: 600px; height: 400px; margin: 0 auto'></div></td>
		</tr>
	";
}

print <<END;
	</table>
	</body>
</html>
END

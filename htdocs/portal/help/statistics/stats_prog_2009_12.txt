# -- 
# AccessLogInfos Report v0.3
# -- 
# Mobyle  Log file: ['/var/log/Mobyle/access_log', '/var/log/Mobyle/bkp/access_log']
# Period 2009/12/01-2009/12/31
HCA                 :  632 jobs  46.00%  301 unique IPs
admetox             :  204 jobs  14.85%   30 unique IPs
PEP-FOLD            :  165 jobs  12.01%   36 unique IPs
PredAcc             :   99 jobs   7.21%    8 unique IPs
wwLig-CSRre         :   32 jobs   2.33%   14 unique IPs
p-sea               :   25 jobs   1.82%    6 unique IPs
psipred             :   18 jobs   1.31%   11 unique IPs
OpenBabel           :   17 jobs   1.24%   12 unique IPs
fpocket2            :   17 jobs   1.24%    3 unique IPs
Modeller            :   14 jobs   1.02%    1 unique IPs
squizz_convert      :   11 jobs   0.80%    2 unique IPs
QMean               :   10 jobs   0.73%    2 unique IPs
fpocket             :   10 jobs   0.73%    4 unique IPs
hpocket             :   10 jobs   0.73%    2 unique IPs
SSpro               :    9 jobs   0.66%    8 unique IPs
PASS                :    8 jobs   0.58%    7 unique IPs
DeSalt              :    7 jobs   0.51%    3 unique IPs
LogP                :    7 jobs   0.51%    5 unique IPs
iSuperpose          :    7 jobs   0.51%    2 unique IPs
CS-Blast            :    5 jobs   0.36%    2 unique IPs
ProbCons            :    5 jobs   0.36%    5 unique IPs
jmol_example        :    5 jobs   0.36%    2 unique IPs
PCE-pKa             :    4 jobs   0.29%    3 unique IPs
PDBBlast2           :    4 jobs   0.29%    3 unique IPs
extractTurn         :    4 jobs   0.29%    4 unique IPs
mdpocket            :    4 jobs   0.29%    1 unique IPs
AutomatP            :    3 jobs   0.22%    2 unique IPs
Frog                :    3 jobs   0.22%    2 unique IPs
HMDPocket           :    3 jobs   0.22%    3 unique IPs
stride              :    3 jobs   0.22%    2 unique IPs
Ali2Modeller        :    2 jobs   0.15%    1 unique IPs
Dihedrals           :    2 jobs   0.15%    2 unique IPs
JME                 :    2 jobs   0.15%    2 unique IPs
MIR                 :    2 jobs   0.15%    2 unique IPs
PCE-pot             :    2 jobs   0.15%    1 unique IPs
SCWRL               :    2 jobs   0.15%    1 unique IPs
coudes              :    2 jobs   0.15%    2 unique IPs
primersearch        :    2 jobs   0.15%    1 unique IPs
ASA                 :    1 jobs   0.07%    1 unique IPs
CysState            :    1 jobs   0.07%    1 unique IPs
ProFitv2_6          :    1 jobs   0.07%    1 unique IPs
XMLBlastSeq         :    1 jobs   0.07%    1 unique IPs
biosed              :    1 jobs   0.07%    1 unique IPs
cons                :    1 jobs   0.07%    1 unique IPs
edialign            :    1 jobs   0.07%    1 unique IPs
hmoment             :    1 jobs   0.07%    1 unique IPs
infoalign           :    1 jobs   0.07%    1 unique IPs
plotcon             :    1 jobs   0.07%    1 unique IPs
prettyplot          :    1 jobs   0.07%    1 unique IPs
prophecy            :    1 jobs   0.07%    1 unique IPs
wobble              :    1 jobs   0.07%    1 unique IPs
# --------------------------------------------------------------------
Total               : 1374 jobs (511 unique IPs)

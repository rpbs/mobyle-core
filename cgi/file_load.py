#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi

def job_suburls(job):
    r = [job['jobID']]
    for j in job.get('subjobs',[]) or []:
        r += job_suburls(j)
    return r

def process( self ):
    url = self.request.getfirst('url')
    if self.session:
        jobs = self.session.getAllJobs()
        urls = [self.session.url]
        for job in jobs:
            urls += job_suburls(job)
        if url.rpartition('/')[0] in urls:
            self.url = url

if __name__ == "__main__":
    mb_cgi.ProxyCGI(processFunction=process,useSession=True)
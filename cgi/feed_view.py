#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi

def process(self):
    # program exists on the Server
    self.xmlUrl = self.request.getfirst('url')
    self.xslPipe = [(self.cfg.portal_path()+"/xsl/"+self.request.getfirst('fmt')+".xsl",{'htdocsDir':"'"+str(self.cfg.portal_url(True))+"'"})]

if __name__ == "__main__":
    il = mb_cgi.XSLCGI(processFunction=process)
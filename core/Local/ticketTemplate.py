####################################################################
#
#  email is send From the user to the mobyle help adress to request some help 
# 
# the available keys are:
# USER     : the user email address
# SENDER   : the email address which sends the mail as defined in the configuration 
# HELP     : the email of the mobyle "hot line" as defined in the configuration
# MSG      : the message written by the USER 
# SERVER_NAME   : the url of the mobyle portal
# SESSION_ID    : the session unique identifier
# SESSION_EMAIL : the user email store in the session
# SESSION_ACTIVATED : if the session is activated
# SESSION_AUTHENTICATED : if the session is authenticated
# JOB_URL         : the url of the job
# JOB_DATE        : the date at which this job was launch
# JOB_STATUS      : the mobyle job status ( finished, error ... )
# JOB_ERROR_PARAM : if there is an error, the parameter name that provoke the error
# JOB_ERROR_MSG   : the error message associated with the error
####################################################################

HELP_REQUEST = """
Date: %(DATE)s
From: %(USER)s
To: %(RECIPIENT)s
Subject: help on job %(JOB_URL)s.

%(MSG)s

Session information:
- id: %(SESSION_ID)s
- email: %(SESSION_EMAIL)s
- activated?: %(SESSION_ACTIVATED)s
- authenticated?: %(SESSION_AUTHENTICATED)s

Job information:
- id: %(JOB_URL)s
- date: %(JOB_DATE)s
- status: %(JOB_STATUS)s
- user error in parameter: %(JOB_ERROR_PARAM)s
- user error message: %(JOB_ERROR_MSG)s
"""

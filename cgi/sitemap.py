#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
import os
import datetime
from Mobyle.Registry import registry

def process(self):
    self.template_file = "sitemap.xml"
    csl = []
    for program in registry.programs:
        try:
            cs = {}
            cs['name'] = program.name
            # the date below is in utc time, with no local time handling so that we do not have to import additional modules
            cs['mtime'] = datetime.datetime.utcfromtimestamp(os.path.getmtime(program.path)).isoformat() + '+00:00'
            csl.append(cs)
        except OSError:
            pass
    self.response['baseurl'] = self.cfg.cgi_url()+'/portal.py'
    self.response['programs'] = csl
      
if __name__ == "__main__":
    mb_cgi.TALCGI(processFunction=process)
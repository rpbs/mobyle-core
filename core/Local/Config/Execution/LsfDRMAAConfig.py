########################################################################################
#                                                                                      #
#   Author: Bertrand Neron,                                                            #
#   Organization:'Biological Software and Databases' Group, Institut Pasteur, Paris.   #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

import logging 
_log = logging.getLogger(__name__)
from Mobyle.MobyleError import MobyleError
from DRMAAConfig import DRMAAConfig

class LsfDRMAAConfig( DRMAAConfig ):
    
        def __init__( self , drmaa_library_path , server_name= '' , lsf_envdir = None , lsf_serverdir = None , nativeSpecification = ''):
            if lsf_envdir is None or lsf_serverdir is None:
                msg = "lsf_envdir and lsf_serverdir must be specified for LsfDRMAAConfig instanciation in Config.EXECUTION_SYSTEM_ALIAS"
                _log.critical( msg )
                raise MobyleError( msg )
            super( LsfDRMAAConfig , self ).__init__( drmaa_library_path, nativeSpecification = nativeSpecification  )
            self.contactString = server_name
            self.envdir = lsf_envdir
            self.serverdir = lsf_serverdir
                        
            
        
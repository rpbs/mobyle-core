// do something groovy in jalview
print "Hello World.\n";
def alf = Jalview.getAlignframes();
for (ala in alf)
{
	// ala is an jalview.gui.AlignFrame object 
	print ala.getTitle()+"\n";
	// get the parent jalview.datamodel.Alignment from the alignment viewport
	def alignment = ala.viewport.alignment;
	// get the first sequence from the jalview.datamodel.Alignment object
	def seq = alignment.getSequenceAt(0); 
}

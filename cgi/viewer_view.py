#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.Registry import registry
import os

def process(self):
    self.xmlUrl = registry.serversByName['local'].viewersByName[self.request.getfirst('program')].url
    self.dataUrl = '%s/viewer_xml_input.py?%s' % (self.cfg.cgi_url(),os.environ['QUERY_STRING'])
    self.xslPipe = [
                    (self.cfg.portal_path()+"/xsl/viewer.xsl",
                        {'jobUrl':"'"+self.dataUrl+"'",
                        'htdocs':"'/"+self.cfg.portal_url(True)+"/'",
                        'codebase':"'"+self.cfg.repository_url()+"/'"}),
                    (self.cfg.portal_path()+"/xsl/remove_ns.xsl",{}) # remove xhtml namespace junk
                    ]
    
if __name__ == "__main__":
    il = mb_cgi.XSLCGI(processFunction=process)
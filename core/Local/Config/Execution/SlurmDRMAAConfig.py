########################################################################################
#                                                                                      #
#   Author: Julien Rey,                                                                #
#   Organization:'MTi' Laboratory, Universite Paris Diderot, Paris.                    #
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

import logging 
_log = logging.getLogger(__name__)

from ExecutionConfig import ExecutionConfig

class SlurmDRMAAConfig( ExecutionConfig ):
    
    def __init__( self, drmaa_library_path , cluster = '', account = '', partition = '', qos = '', slurm_opts = '' ):
        super( SlurmDRMAAConfig , self ).__init__()
        self.drmaa_library_path = drmaa_library_path
        self.contactString = '' 
        self.cluster = cluster
        self.account = account
        self.partition = partition
        self.qos = qos
        self.slurm_opts = slurm_opts

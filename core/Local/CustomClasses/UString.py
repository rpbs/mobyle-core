########################################################################################
#                                                                                      #
#   Author: Bertrand Neron,                                                            #
#   Organization:'Biological Software and Databases' Group, Institut Pasteur, Paris.   #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

"""
Content the basics Mobyle Parameter types 
"""

import os 
import re
from types import BooleanType , StringTypes
from logging import getLogger
c_log = getLogger(__name__)
b_log = getLogger('Mobyle.builder')

import shutil

from Mobyle.Utils import safeFileName
from Mobyle.Classes.DataType import DataType
from Mobyle.Service import MobyleType

from Mobyle.MobyleError import MobyleError , UserValueError , UnDefAttrError , UnSupportedFormatError
from Mobyle.ConfigManager import Config
_cfg = Config()


class UStringDataType( DataType ):

    def convert( self , value , acceptedMobyleType , detectedMobyleType = None , paramFile= False ):
        """
        Try to cast the value to String. 
        
        @param value: the value provided by the User for this parameter
        @type value: 
        @return: the value converted to String.
        @rtype: String
        @param acceptedMobyleType: the MobyleType  accepted by this service parameter 
        @type acceptedMobyleType: L{MobyleType} instance
        @param detectedMobyleType: the MobyleType discribing this data
        @type detectedMobyleType: L{MobyleType} instance
        @raise UserValueError: if the cast fails.
        """
        if value is None:
            return ( None , acceptedMobyleType )
        #type controls
        try:
            value = str( value ).encode('ascii')
        except ValueError :
            raise UserValueError( msg = "should be an (ascii) string" ) 
        #strings with space are allowed, but if the string will appear
        #as shell instruction it must be quoted
        if value.find(' ') != -1 and not paramFile:
            value = "'%s'" % value
        return ( value , acceptedMobyleType )                        
        
    def detect( self , value ):
        mt = MobyleType( self )
        return mt        
    
    def validate( self, param  ):
        """
        @return: True if the value is a string and doesnot contain dangerous characters.
        ( allowed characters: the words , space, ' , - , + , and dot if is not followed by another dot
        and eventually surrounded by commas)
        @rtype: boolean
        @raise UserValueError: if the value does not statisfy security regexp.
        """
        value = param.getValue()
        if value is None:
            return True
        
        #allowed characters:
        #the words , space, ' , - , + , and dot if is not followed by another dot 
        #and eventually surrounded by commas
        reg = "(\w|\ |-|\+|,|@|\:|\.(?!\.))+"
        if re.search( "^(%s|'%s')$" % (reg, reg) ,  value ) :         
            return True
        else:
            msg = "this value: \"" + str( value ) + "\" , is not allowed"
            raise UserValueError( parameter = param , msg = msg )
        
    
        


var CookieConsent = Class.create({
  
    initialize: function(el) {
        this.banner = undefined
    },

    createBanner: function() {
        this.banner = document.createElement("div");
        this.banner.innerHTML = `<div id="cookieconsent-banner" class="popup_overlay">
                                   <span id="insertHere" class="popup_background"></span>
                                   <form name="usercookieForm" class="modal">
                                     <strong style="color:red; font-weight:bold;">This website uses cookies</strong>
                                     <hr>
                                     <p style="font-size: 18px">This website uses only essential cookies to ensure its proper operation. No tracking or analysis of these cookies are performed.</p>
                                     <input type="button" id="acceptcookies" value="Consent">
                                   </form>
                              </div>`;
        document.body.appendChild(this.banner);

        document.getElementById('acceptcookies').addEventListener('click', function() {
            document.getElementById('cookieconsent-banner').style.display = 'none';
        })
    },

})


window.onload = function () {
    var cookieconsent = new CookieConsent()
    cookieconsent.createBanner()
}

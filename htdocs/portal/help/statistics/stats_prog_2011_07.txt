# -- 
# AccessLogInfos Report v0.3
# -- 
# Mobyle  Log file: ['/var/log/Mobyle/access_log', '/var/log/Mobyle/bkp/access_log']
# Period 2011/07/01-2011/07/31
FAF-Drugs           :  539 jobs  25.57%   46 unique IPs
HCA                 :  464 jobs  22.01%  218 unique IPs
PEP-FOLD            :  353 jobs  16.75%   85 unique IPs
FAF-Drugs2          :  153 jobs   7.26%   30 unique IPs
OpenBabel           :  134 jobs   6.36%   25 unique IPs
iSuperpose          :  110 jobs   5.22%    7 unique IPs
Bank-Formatter      :   60 jobs   2.85%   11 unique IPs
PASS                :   37 jobs   1.76%   27 unique IPs
Frog2               :   34 jobs   1.61%    4 unique IPs
psipred             :   32 jobs   1.52%    8 unique IPs
mdpocket            :   26 jobs   1.23%    6 unique IPs
wwLig-CSRre         :   26 jobs   1.23%    9 unique IPs
ASA                 :   12 jobs   0.57%    7 unique IPs
LogP                :   12 jobs   0.57%    5 unique IPs
PredAcc             :   10 jobs   0.47%    6 unique IPs
fpocket2            :   10 jobs   0.47%    7 unique IPs
fpocket             :    7 jobs   0.33%    6 unique IPs
SSpro               :    6 jobs   0.28%    4 unique IPs
dnadist             :    6 jobs   0.28%    2 unique IPs
squizz_checker      :    6 jobs   0.28%    2 unique IPs
DeSalt              :    5 jobs   0.24%    2 unique IPs
coudes              :    5 jobs   0.24%    2 unique IPs
Dihedrals           :    4 jobs   0.19%    4 unique IPs
Frog                :    4 jobs   0.19%    2 unique IPs
JME                 :    4 jobs   0.19%    3 unique IPs
PCE-pot             :    4 jobs   0.19%    2 unique IPs
ProbCons            :    4 jobs   0.19%    2 unique IPs
extractTurn         :    4 jobs   0.19%    3 unique IPs
PoPMuSic            :    3 jobs   0.14%    2 unique IPs
CysState            :    2 jobs   0.09%    1 unique IPs
GLSearch            :    2 jobs   0.09%    1 unique IPs
HMDPocket           :    2 jobs   0.09%    1 unique IPs
HMMEncode           :    2 jobs   0.09%    2 unique IPs
PCE-pKa             :    2 jobs   0.09%    2 unique IPs
ProFitv2_6          :    2 jobs   0.09%    2 unique IPs
TEF                 :    2 jobs   0.09%    2 unique IPs
jmol_example        :    2 jobs   0.09%    2 unique IPs
plotSC              :    2 jobs   0.09%    2 unique IPs
stride              :    2 jobs   0.09%    2 unique IPs
AMMOS               :    1 jobs   0.05%    1 unique IPs
AddHydrogens        :    1 jobs   0.05%    1 unique IPs
DG-AMMOS            :    1 jobs   0.05%    1 unique IPs
H-bonds             :    1 jobs   0.05%    1 unique IPs
HHSearch            :    1 jobs   0.05%    1 unique IPs
MINI-FOLD           :    1 jobs   0.05%    1 unique IPs
antigenic           :    1 jobs   0.05%    1 unique IPs
drawgram            :    1 jobs   0.05%    1 unique IPs
einverted           :    1 jobs   0.05%    1 unique IPs
findkm              :    1 jobs   0.05%    1 unique IPs
garnier             :    1 jobs   0.05%    1 unique IPs
p-sea               :    1 jobs   0.05%    1 unique IPs
pepnet              :    1 jobs   0.05%    1 unique IPs
pepwheel            :    1 jobs   0.05%    1 unique IPs
# --------------------------------------------------------------------
Total               : 2108 jobs (568 unique IPs)


########################################################################################
#                                                                                      #
#   Author: Julien Rey,                                                                #
#   Organization:'MTi' Laboratory, Universite Paris Diderot, Paris.                    #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

import os
import subprocess
import socket
import logging
_log = logging.getLogger(__name__)

from Mobyle.Execution.SlurmDRMAA import SlurmDRMAA

from Mobyle.MobyleError import MobyleError

class SlurmAcctDRMAA(SlurmDRMAA):
    """
    Run the commandline with batch system DRMAA bindings
    """  
    def __init__( self, drmaa_config ):
        remoteAddr = os.environ.get('REMOTE_ADDR')
        account = self._check_slurm_account(remoteAddr)
        drmaa_config.accountName = account
        super( SlurmAcctDRMAA , self ).__init__( drmaa_config )

    def _check_slurm_account( self, remoteAddr ):
        """
        Check if client IP has an account in sacctmgr, if not, creates it
        @param remoteAddr: ip address for the account
        @type remoteAddr: string
        """
        try:
            socket.inet_aton( remoteAddr )
        except socket.error:
            msg = "%s is not a valid IP address." % remoteAddr
            _log.error( msg )
            raise MobyleError, msg

        accountName = remoteAddr

        try:
            sacctmgr_output = subprocess.check_output(["/usr/local/bin/sacctmgr", "-n", "-P", "show", "account", accountName])
        except OSError, err:
            msg = "Error when querying the slurm account database: " + str(err)
            _log.error( msg )
            raise MobyleError, msg

        if accountName != sacctmgr_output.split('|')[0]:
            self._add_slurm_account( accountName )

        return accountName

    def _add_slurm_account( self, accountName ):
        """
        Add a slurm account
        @param accountName: name for the account
        @type accountName: string
        """
        try:
            subprocess.call(["/usr/local/bin/sacctmgr",  "-i", "create", "account", accountName, 'parent=mobyle', 'cluster=cluster', 'fairshare=10'])
        except Exception, err:
            msg = "Error when creating account %s: %s" % ( accountName, err )
            _log.error( msg )
            raise MobyleError, msg

        userName = os.environ.get('USER', 'www-data')

        try:
            subprocess.call(["/usr/local/bin/sacctmgr",  "-i", "create", "user", userName, 'account=%s' % accountName, 'cluster=cluster'])
        except Exception, err:
            msg = "Error when associating user %s with account %s: %s" % ( userName, accountName, err )
            _log.error( msg )
            raise MobyleError, msg

        return

#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.Registry import registry
from Mobyle.ClassificationIndex import ClassificationIndex
from Mobyle.SearchIndex import SearchIndex
from Mobyle.DescriptionsIndex import DescriptionsIndex


def process(self):
    self.template_file = "programs_list.html"
    self.response['filterOn'] = 'false'
    self.response['emptySearchResults'] = 'false'
    
    if self.request.getfirst('plSubmit')=='Search':
        searchString = self.request.getfirst('searchString',None)
        if searchString:
            self.response['filterOn'] = 'true'
            si = SearchIndex("program")
            si.filterRegistry(searchString.split(' '))
            sj = SearchIndex("workflow")
            sj.filterRegistry(searchString.split(' '))
            st = SearchIndex("tutorial")
            st.filterRegistry(searchString.split(' '))
        if len(registry.programs)+len(registry.workflows)==0:
            self.response['emptySearchResults'] = 'true'

    p_classification = ClassificationIndex("program")
    w_classification = ClassificationIndex("workflow")
    t_classification = ClassificationIndex("tutorial")
    DescriptionsIndex("program").fillRegistry()
    DescriptionsIndex("workflow").fillRegistry()
    DescriptionsIndex("tutorial").fillRegistry()
    p_classification.buildRegistryCategories(field=self.request.getfirst('classifyBy','category'),serviceTypeSort=self.request.getfirst('serviceTypeSort','separate'))
    w_classification.buildRegistryCategories(field=self.request.getfirst('classifyBy','category'),serviceTypeSort=self.request.getfirst('serviceTypeSort','separate'))
    t_classification.buildRegistryCategories(field=self.request.getfirst('classifyBy','category'),serviceTypeSort=self.request.getfirst('serviceTypeSort','separate'))
    registry.servers.sort()
    self.response['registry']=registry
      
if __name__ == "__main__":
    mb_cgi.TALCGI(processFunction=process,useSession=True)
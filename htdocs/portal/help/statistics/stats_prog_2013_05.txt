# -- 
# AccessLogInfos Report v0.3
# -- 
# Mobyle  Log file: ['/var/log/mobyle/access_log.all']
# Period 2013/05/01-2013/05/31
PEP-FOLD            : 2518 jobs  47.03%  377 unique IPs
FAF-Drugs2          :  794 jobs  14.83%   60 unique IPs
HCA                 :  760 jobs  14.19%  225 unique IPs
Bank-Formatter      :  202 jobs   3.77%   29 unique IPs
SABBAC              :  132 jobs   2.47%   15 unique IPs
PDBBlast2           :   70 jobs   1.31%   31 unique IPs
fpocket             :   66 jobs   1.23%   13 unique IPs
psipred             :   62 jobs   1.16%   19 unique IPs
T-Coffee            :   56 jobs   1.05%    1 unique IPs
stride              :   44 jobs   0.82%    7 unique IPs
FAF-Drugs2_test     :   40 jobs   0.75%    4 unique IPs
Frog2               :   38 jobs   0.71%   11 unique IPs
cbs.Atome2          :   36 jobs   0.67%   11 unique IPs
PepBasic3D          :   32 jobs   0.60%   11 unique IPs
mdpocket            :   32 jobs   0.60%    5 unique IPs
MarvinSketch        :   28 jobs   0.52%    2 unique IPs
wwLig-CSRre         :   28 jobs   0.52%    8 unique IPs
PepDock             :   26 jobs   0.49%    4 unique IPs
lipm.iANTPatScan    :   26 jobs   0.49%    8 unique IPs
plotSC              :   26 jobs   0.49%    3 unique IPs
PepSolubility       :   24 jobs   0.45%   11 unique IPs
ASA                 :   22 jobs   0.41%    7 unique IPs
lipm.SeqLogo        :   18 jobs   0.34%    5 unique IPs
Gromacs_Minimizer   :   16 jobs   0.30%    7 unique IPs
Homodel             :   14 jobs   0.26%    5 unique IPs
MIR                 :   14 jobs   0.26%    5 unique IPs
Annot3D             :   12 jobs   0.22%    6 unique IPs
HHSearch            :   12 jobs   0.22%    4 unique IPs
PEPFOLDTRJ          :   12 jobs   0.22%    5 unique IPs
TEF                 :   12 jobs   0.22%    4 unique IPs
PPP                 :   10 jobs   0.19%    5 unique IPs
ipmc.XLogP          :   10 jobs   0.19%    3 unique IPs
muscle              :   10 jobs   0.19%    3 unique IPs
pasteur.clustalw-multialign:   10 jobs   0.19%    2 unique IPs
H-bonds             :    8 jobs   0.15%    2 unique IPs
iSuperpose          :    8 jobs   0.15%    4 unique IPs
oscar-star          :    8 jobs   0.15%    4 unique IPs
AMMOS               :    6 jobs   0.11%    2 unique IPs
DG-AMMOS            :    6 jobs   0.11%    2 unique IPs
Filter-Editor       :    6 jobs   0.11%    3 unique IPs
Modeller9           :    6 jobs   0.11%    3 unique IPs
Refine              :    6 jobs   0.11%    3 unique IPs
SAFrag              :    6 jobs   0.11%    3 unique IPs
SolyPep             :    6 jobs   0.11%    3 unique IPs
extractTurn         :    6 jobs   0.11%    3 unique IPs
lipm.InterproAnnotation:    6 jobs   0.11%    2 unique IPs
p-sea               :    6 jobs   0.11%    3 unique IPs
Clusterize          :    4 jobs   0.07%    1 unique IPs
DeSalt              :    4 jobs   0.07%    2 unique IPs
GLSearch            :    4 jobs   0.07%    1 unique IPs
PDBBlast            :    4 jobs   0.07%    2 unique IPs
Yakusa              :    4 jobs   0.07%    1 unique IPs
avp                 :    4 jobs   0.07%    2 unique IPs
basicbuilder        :    4 jobs   0.07%    1 unique IPs
cbs.eval23d         :    4 jobs   0.07%    2 unique IPs
chemogenomics.Shaper:    4 jobs   0.07%    2 unique IPs
chemogenomics.SiteAlign:    4 jobs   0.07%    1 unique IPs
chemogenomics.VolSite:    4 jobs   0.07%    1 unique IPs
hpocket             :    4 jobs   0.07%    1 unique IPs
Dihedrals           :    2 jobs   0.04%    1 unique IPs
PepSeq              :    2 jobs   0.04%    1 unique IPs
REMD-OPEP           :    2 jobs   0.04%    1 unique IPs
SMARTS_Testing      :    2 jobs   0.04%    1 unique IPs
chemogenomics.FuzCav:    2 jobs   0.04%    1 unique IPs
# --------------------------------------------------------------------
Total               : 5354 jobs (975 unique IPs)

<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  
  <xsl:include href="job.xsl" />
  
  <xsl:param name="dataId" />

  <xsl:param name="sessionUrl" />  

  <xsl:param name="inputModes" />  
  
  <xsl:template match="/">
    <xsl:apply-templates select="//data[@id=$dataId]" />
  </xsl:template>

  <xsl:template match="data">
    <fieldset class="bookmark" data-bookmarkpid="{@id}"
      data-pid="{@id}"
      data-format="{type/dataFormat/text()}" 
      data-datatype="{type/datatype/class/text()}" 
      data-biotype="{type/biotype/text()}" 
      data-card="{type/card/text()}" 
      data-inputmodes="{$inputModes}"
      data-username="{userName/text()}">
      <legend>
        <xsl:value-of select="userName/text()"/>
        <xsl:if test="type/dataFormat/text()">
          (<xsl:value-of select="type/dataFormat/text()"/>)
        </xsl:if>
        <a target="_blank" class="saveFileLink" title="save this file" alt="save this file" href="{$sessionUrl}/{@id}?save"> save </a>
      </legend>
      <div class="parameter" 
        data-parametername="" 
        data-datatype-superclass="" 
        data-format="{type/dataFormat/text()}" 
        data-datatype="{type/datatype/class/text()}" 
        data-biotype="{type/biotype/text()}" 
        data-card="{type/card/text()}" data-inputmodes="{$inputModes}">
        <span data-filename="{@id}" data-src="{$sessionUrl}/{@id}">
          <xsl:choose>  
            <xsl:when test="@size&lt;=$previewDataLimit">
                <xsl:text disable-output-escaping="yes">&lt;![if !IE]&gt;</xsl:text>
              <object data="{$sessionUrl}/{@id}">
                This file cannot be displayed in your browser. Click on the "save" link to download it.                  
              </object>
              <xsl:text disable-output-escaping="yes">&lt;![endif]&gt;</xsl:text>              
              <xsl:text disable-output-escaping="yes">&lt;!--[if IE]&gt;</xsl:text>
              <iframe src="{$sessionUrl}/{@id}">
                This file cannot be displayed in your browser. Click on the "save" link to download it.                  
              </iframe>
              <xsl:text disable-output-escaping="yes">&lt;![endif]--&gt;</xsl:text>              
            </xsl:when>
            <xsl:otherwise>
              <p class="commentText">
                The file is too big to be safely displayed here (<xsl:value-of select="round(number(@size) div 1024)"/> KiB).
                <a target="_blank" href="{$sessionUrl}/{@id}">Click here to display this result in a separate window.</a>
              </p>
            </xsl:otherwise>
          </xsl:choose>
        </span>
      </div>
    </fieldset>
  </xsl:template>

</xsl:stylesheet>
########################################################################################
#                                                                                      #
#   Author: Bertrand Neron,                                                            #
#   Organization:'Biological Software and Databases' Group, Institut Pasteur, Paris.   #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

import glob 
import os 

MOBYLEHOME = '/opt/mobyle/core'
if os.environ.has_key('MOBYLEHOME'):
    MOBYLEHOME = os.environ['MOBYLEHOME']
if not MOBYLEHOME:
    import sys
    sys.exit('MOBYLEHOME must be defined in your environment')

for _file in glob.glob( os.path.join( MOBYLEHOME , 'Local' , 'Config' , 'Execution' , '*Config.py' ) ):
    module_name = os.path.splitext( os.path.basename( _file ) )[0]
    if module_name != '__init__':
        try: 
            module = __import__( module_name , globals(), locals(), [ module_name ])
            klass = getattr( module , module_name)
            locals()[ module_name ] = klass
        except Exception , err :
            print >> sys.stderr , "the module %s cannot be loaded : %s" %( module_name , err )
            continue
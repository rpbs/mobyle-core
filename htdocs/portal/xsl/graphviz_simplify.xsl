<?xml version="1.0" encoding="utf-8"?>
<!-- 
	form_graph.xsl stylesheet
	Authors: Hervé Ménager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:svg="http://www.w3.org/2000/svg">

	<xsl:include href="ident.xsl" />

	<xsl:template match="svg:*/@style" />
	<xsl:template match="svg:*/@stroke" />
	<xsl:template match="svg:*/@font-family" />
	<xsl:template match="svg:*/@font-size" />
	<xsl:template match="svg:svg/@width" />
	<xsl:template match="svg:svg/@height" />	
	
	<xsl:template match="interface[@type='graph']/svg:svg">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:variable name="w"><xsl:value-of select="number(substring-before(@width,'pt'))"/></xsl:variable>
			<xsl:variable name="h"><xsl:value-of select="number(substring-before(@height,'pt'))"/></xsl:variable>
			<xsl:variable name="width">
				<xsl:choose>
					<xsl:when test="$w &lt; 800">800</xsl:when>
					<xsl:otherwise><xsl:value-of select="$w"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="viewBox"><xsl:text>0 0 </xsl:text><xsl:value-of select="$width" /><xsl:text> </xsl:text><xsl:value-of select="$h" /></xsl:attribute>
			<style>
				g{
					fill: white; stroke:black; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 14px;
				}

				text{
					fill: black; stroke: none;
				}				

                .jobstatus.building text{
                    fill: darkorange;
                }
                
                .jobstatus.submitted text, .jobstatus.pending text, .jobstatus.running text, .jobstatus.hold text{
                    fill: darkorange;
                }
                
                .jobstatus.error text, .jobstatus.killed text{
                    fill: red;
                }
                
                .jobstatus.finished text{
                    fill: green;
                }
                
                a.jobstatus:hover{
                	stroke-width: 2px;
                }
            </style>
			<xsl:apply-templates select="node()|text()" />
		</xsl:copy> 
	</xsl:template>	

</xsl:stylesheet>

########################################################################################
#                                                                                      #
#   Author: Bertrand Neron,                                                            #
#   Organization:'Biological Software and Databases' Group, Institut Pasteur, Paris.   #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

import logging 
_log = logging.getLogger(__name__)

class ExecutionConfig(object):
    def __init__( self ):
        self.execution_class_name =  self.__class__.__name__[:-6]
    

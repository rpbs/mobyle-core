#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi, re
import PyPDB.PyPDB as PDB
from subprocess import Popen, PIPE

def getLigands( self ):
    if not(self.request.has_key('pdb')):
        self.jsonMap['errormsg'] = "Please specify a PDB file."
    else:
        pdb = self.request.getfirst('pdb').split("\n")
        x = PDB.PDB(pdb)
        rs = []
        for i in x:
            if i.rType()=="HETERO":
                ligand = "[%s]%s:%s" % (i.rName(), i.rNum(), i.chnLbl())
                if ligand not in rs:
                    rs.append(ligand)
        self.jsonMap['content'] = rs

if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=getLigands)

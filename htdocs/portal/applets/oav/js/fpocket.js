
function js_pocket_surface(obj, id) {
  if(obj.checked){
    if(!obj.surface){
      av_execute('define prevsel current ; select none ; surface -solid true pocket'+id+'surf white group prot'+id+'; select group prevsel ;');
      obj.surface = true;
    }else{
      av_display('pocket'+id+'surf', 'on');
    }
  }else{
    av_display('pocket'+id+'surf', 'off');
  }
}

function js_pocket_surface_color(obj, id) {
  var val = obj.options[obj.selectedIndex].value;
  av_colour('pocket'+id+'surf', val);
}

function js_pocket_surface_transparency(obj, id) {
  var val = obj.options[obj.selectedIndex].value;
  val = Math.round(val * 2.55);
  av_transparency('pocket'+id+'surf', val);
}

function js_pocket_envelope(obj, id) {
  if(obj.checked){
    if(!obj.surface){
      av_execute('define prevsel current ; select none ; surface -solid false pocket'+id+'env green group as'+id+'; select group prevsel ;');
      obj.surface = true;
    }else{
      av_display('pocket'+id+'env', 'on');
    }
  }else{
    av_display('pocket'+id+'env', 'off');
  }
}

function js_pocket_envelope_color(obj, id) {
  var val = sel.options[sel.selectedIndex].value;
  av_transparency('pocket'+id+'env', val);
}

function js_pocket_envelope_transparency(obj, id) {
  var val = obj.options[obj.selectedIndex].value;
  val = Math.round(val * 2.55);
  av_colour('pocket'+id+'env', val);
}

#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.JobFacade import JobFacade
from Mobyle.MobyleError import MobyleError

def process(self):
    try:
        j = JobFacade.getFromService(programUrl=self.request.getfirst('programName',None),workflowUrl=self.request.getfirst('workflowUrl',None))
    except MobyleError, err:
        self.jsonMap = {'errormsg': err.message}
        return
    self.jsonMap = j.create(self.request)
    
if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process)
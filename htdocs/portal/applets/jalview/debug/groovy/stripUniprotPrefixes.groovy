import jalview.analysis.*;
import jalview.datamodel.*;
import jalview.gui.AlignFrame;
import jalview.gui.AlignViewport;

def af = Jalview.getAlignframes();

// walk through  all alignments, stripping off all text prior to and including last '|' symbol in sequence IDs

for (ala in af)
{
	def al = ala.viewport.alignment;
	if (al!=null)
	{
		SequenceI[] seqs = al.getSequencesArray();
		for (sq in seqs)
		{
			if (sq!=null) {
				if (sq.getName().indexOf("|")>-1)
				{
					sq.setName(sq.getName().substring(sq.getName().lastIndexOf("|")+1));
					if (sq.getDatasetSequence()!=null)
					{
						sq.getDatasetSequence().setName(sq.getName());
					}
				}
			}
		}
	}
}
	
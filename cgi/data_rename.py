#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi

def process( self ):
    self.message = ""
    if self.session:
        bookmark_id = self.request.getfirst('id')
        userName = self.request.getfirst('userName').strip()
        if(self.session.hasData(bookmark_id)):
            self.session.renameData(bookmark_id,userName)
            self.message = userName

if __name__ == "__main__":
    mb_cgi.TextCGI(processFunction=process,useSession=True)
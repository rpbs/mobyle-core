#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
import workflow

def process(self):
    self.template_file = 'workflow_test.html'
    workflow.process(self)
        
if __name__ == "__main__":
    mb_cgi.TALCGI(processFunction=process,useSession=True)
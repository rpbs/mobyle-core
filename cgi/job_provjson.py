#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
from time import strftime, strptime

import mb_cgi
from Mobyle.JobFacade import JobFacade
from Mobyle.JobState import JobState

def process(self):
    j = JobFacade.getFromJobId(self.request.getfirst('jobId',None))
    js = JobState(j.jobId)
    status = j.getStatus()
    job_date = strptime(  js.getDate() , "%x  %X")
    job_sdate = strftime("%Y-%m-%dT%H:%M:%S+0000", job_date)
    user = {
            "@id": "mbox:%s" % js.getEmail(),
            "@type": "prov:Person",
            "foaf:mbox": js.getEmail()
           }
    inputs = []
    usages = []
    for if_t in js.getInputFiles():
        parameter_name = if_t[0]
        for input_file in if_t[1]:
            i = {
                "@id": input_file[0],
                "@type": "prov:Entity",
                "wasAttributedTo": user["@id"]
            }
            u = { 
                "entity": input_file[0],
                "hadRole": "%s#%s" % (j.jobId, parameter_name)
            }
            inputs.append(i)
            usages.append(u)
    outputs = []
    for of_t in js.getOutputs():
        parameter_name = of_t[0]
        for output_file in of_t[1]:
            o = {
                "@id": output_file[0],
                "@type": "prov:Entity",
                "generation": {
                    "activity": j.jobId,
                    "hadRole": "%s#%s" % (j.jobId, parameter_name)
                }
            }
            outputs.append(o)
    self.jsonMap = {
    "@context": {
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "prov": "http://www.w3.org/ns/prov#",
        "foaf": "http://xmlns.com/foaf/0.1/",

        "agent": { "@type": "@id", "@id": "prov:agent" },
        "entity": { "@type": "@id", "@id": "prov:entity" },
        "activity": { "@type": "@id", "@id": "prov:activity" },
        "hadPlan": { "@type": "@id", "@id": "prov:hadPlan" },
        "hadRole": { "@type": "@id", "@id": "prov:hadRole" },
        "wasAttributedTo": { "@type": "@id", "@id": "prov:wasAttributedTo" },
        "association": { "@type": "@id", "@id": "prov:qualifiedAssociation" },
        "usage": { "@type": "@id", "@id": "prov:qualifiedUsage" },
        "generation": { "@type": "@id", "@id": "prov:qualifiedGeneration" },

        "startedAtTime": { "@type": "xsd:dateTime", "@id": "prov:startedAtTime" },
        "endedAtTime": { "@type": "xsd:dateTime", "@id": "prov:endedAtTime" },

        "@base": j.jobId
    },
    "@graph": [
        {   "@id": j.jobId,
            "@type": "prov:Activity",
            "startedAtTime": job_sdate, #TODO: time zone in python... (+0200)
            #"endedAtTime": "2013-07-17T14:50:59+02:00",
            "association": [
                {
                    "agent": js.getHost(),
                    "hadPlan": js.getName()
                },
            {
                    "agent": "mbox:%s" % js.getEmail()
                }
            ],
            "usage": [usages]
        }, user
    ]+inputs+outputs
}
    
if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process)

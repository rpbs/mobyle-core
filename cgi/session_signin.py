#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
import Mobyle.Net
from Mobyle.MobyleError import SessionError

def process(self):
    try:
        assert self.request.getfirst('email'), "Please provide an e-mail"
        assert self.request.getfirst('password'), "Please provide a password"
    except AssertionError, ae:
        self.error_msg = str(ae)
        return
    try:
        self.anonymousSession = self.session
        email = Mobyle.Net.EmailAddress(self.request.getfirst('email'))
        password = self.request.getfirst('password')
        self.session = self.sessionFactory.getAuthenticatedSession(email,passwd=password)
        if self.anonymousSession:
            self.session.mergeWith(self.anonymousSession)
        self.sessionKey = self.session.getKey()
        self.read_session()
        self.jsonMap['ok'] = str(True)
    except SessionError, e:
        self.jsonMap['ok'] = str(False)
        self.error_msg = str(e)
        
if __name__ == "__main__":
    import os
    mb_cgi.JSONCGI(processFunction=process,useSession=True)

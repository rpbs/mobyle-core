#!/usr/bin/env python
# PDBGet.py
#
# A simple loarder parser for PDB files
#
# Author: P. Tuffery, RPBS, INSERM UMR-S 726, 2008
# Adapted from PDB.py (2001-2008) by P. Tuffery, RPBS, INSERM, France
#
# No warranty of any kind is provided
#
"""
Classe PDBGet pour chercher une entree PDB ou une entree Astral/Scop

Try locally (depends on : GDFLTPDBDIR and GDFLTSCOPDIR)
else: try on the net, for wwPDB or Astral at least.
"""

import sys
import string

# Pour information
GDFLTPDBDIR  = "/scratch/banks/pdb/data/structures/"
GDFLTSCOPDIR = "/scratch/banks/astral/astral_2015-04-10/flat/"

#from PDB6_7 import *
#from PDB import *
import PyPDB.PyPDB as PDB

if __name__ == "__main__":
 	# x = PDB.PDB(string.lower(sys.argv[1]), verbose = 0)
	pdbId = sys.argv[1]
	if sys.argv[1][0] in "123456789":
		pdbId = string.lower(sys.argv[1][0:4])+sys.argv[1][4:]
 	x = PDB.PDB(pdbId, verbose = 0)
	x.out(info = 1, header = 0, allModels = 0, end = 1)
	# x.out()



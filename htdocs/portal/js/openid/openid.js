jQuery.noConflict();

function bindOpenIdForm() {
  jQuery('#openid_form').hide();
  jQuery('#openid_loader').show();
  jQuery('#openid_user_bar').load(
    jQuery('#openid_form').attr('action'),
    { 'openidurl': jQuery("input[id$='_openid_url']").val() },
    function() {
      jQuery('#openid_form').unbind();
      jQuery('#openid_form').submit(function() {return bindOpenIdForm();});
      if (typeof(transiting) == 'undefined' || !transiting) {
        jQuery('#openid_loader').hide();
      }
      else {
        jQuery('#openid_loader').show();
        jQuery('#openid_user_bar').hide();
        transiting = false;
      }
    }
  );
  return false;
}

function changeServer(url, selectStart, selectLength) {
  jQuery("input[id$='_openid_url']").val(url);
  
  // IE
  if (jQuery("input[id$='_openid_url']")[0].createTextRange) {
    var range = jQuery("input[id$='_openid_url']")[0].createTextRange();
    range.collapse(true);
    range.moveEnd('character', selectStart+selectLength);
    range.moveStart('character', selectStart);
    range.select();
  // real browsers :)
  } else if (jQuery("input[id$='_openid_url']")[0].setSelectionRange) {
    jQuery("input[id$='_openid_url']")[0].focus();
    jQuery("input[id$='_openid_url']")[0].setSelectionRange(selectStart, selectStart+selectLength);
  }
    
  return false;
}

  

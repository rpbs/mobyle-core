<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>

	<!--
		general templates copy everything you find in the input, unless
		otherwise specified
	-->
	<xsl:template match="*">
		<!-- copy element -->
		<xsl:copy>
			<!-- copy its attributes -->
			<xsl:apply-templates select="@*" />
			<!-- process its sub-elements -->
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@*">
    <xsl:copy/>
  </xsl:template>	

	<!-- Remove xml:base attributes introduced by XInclude -->
	<xsl:template match="@xml:*" />

	
</xsl:stylesheet>
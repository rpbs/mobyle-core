# -- 
# AccessLogInfos Report v0.3
# -- 
# Mobyle  Log file: ['log/access_log.all']
# Period 2012/09/01-2012/09/30
PEP-FOLD            : 1829 jobs  60.76%  358 unique IPs
FAF-Drugs2          :  474 jobs  15.75%   90 unique IPs
HCA                 :  326 jobs  10.83%   62 unique IPs
fpocket             :   55 jobs   1.83%   15 unique IPs
Bank-Formatter      :   50 jobs   1.66%   29 unique IPs
psipred             :   44 jobs   1.46%   21 unique IPs
wwLig-CSRre         :   26 jobs   0.86%    3 unique IPs
ipmc.PLANTS         :   25 jobs   0.83%    6 unique IPs
PCE-pot             :   18 jobs   0.60%    2 unique IPs
TEF2                :   18 jobs   0.60%   10 unique IPs
Genopept            :   11 jobs   0.37%    3 unique IPs
Gromacs_Minimizer   :    8 jobs   0.27%    7 unique IPs
PEPFOLDTRJ          :    8 jobs   0.27%    6 unique IPs
lipm.SeqLogo        :    8 jobs   0.27%    5 unique IPs
PPP                 :    7 jobs   0.23%    7 unique IPs
extractTurn         :    7 jobs   0.23%    7 unique IPs
ipmc.XLogP          :    7 jobs   0.23%    2 unique IPs
Frog2               :    6 jobs   0.20%    4 unique IPs
H-bonds             :    6 jobs   0.20%    5 unique IPs
HHSearch            :    5 jobs   0.17%    4 unique IPs
PCE-pKa             :    5 jobs   0.17%    4 unique IPs
Yakusa              :    5 jobs   0.17%    3 unique IPs
Dihedrals           :    4 jobs   0.13%    4 unique IPs
Refine              :    4 jobs   0.13%    4 unique IPs
hpocket             :    4 jobs   0.13%    1 unique IPs
AMMOS               :    3 jobs   0.10%    3 unique IPs
MIR                 :    3 jobs   0.10%    3 unique IPs
ipmc.LEA3D          :    3 jobs   0.10%    3 unique IPs
mdpocket            :    3 jobs   0.10%    1 unique IPs
oscar-star          :    3 jobs   0.10%    3 unique IPs
p-sea               :    3 jobs   0.10%    3 unique IPs
stride              :    3 jobs   0.10%    3 unique IPs
workflow_1          :    3 jobs   0.10%    2 unique IPs
workflow_3          :    3 jobs   0.10%    2 unique IPs
Filter-Editor       :    2 jobs   0.07%    2 unique IPs
JME                 :    2 jobs   0.07%    1 unique IPs
MarvinSketch        :    2 jobs   0.07%    1 unique IPs
SABBAC              :    2 jobs   0.07%    2 unique IPs
TEF                 :    2 jobs   0.07%    2 unique IPs
lipm.iANTPatScan    :    2 jobs   0.07%    1 unique IPs
ASA                 :    1 jobs   0.03%    1 unique IPs
Ali2Modeller        :    1 jobs   0.03%    1 unique IPs
DeSalt              :    1 jobs   0.03%    1 unique IPs
HHFrag              :    1 jobs   0.03%    1 unique IPs
PDBBlast2           :    1 jobs   0.03%    1 unique IPs
SMARTS_Testing      :    1 jobs   0.03%    1 unique IPs
avp                 :    1 jobs   0.03%    1 unique IPs
infochimie.molconvert:    1 jobs   0.03%    1 unique IPs
molconvert_test     :    1 jobs   0.03%    1 unique IPs
pasteur.clustalw-multialign:    1 jobs   0.03%    1 unique IPs
plotSC              :    1 jobs   0.03%    1 unique IPs
# --------------------------------------------------------------------
Total               : 3010 jobs (705 unique IPs)

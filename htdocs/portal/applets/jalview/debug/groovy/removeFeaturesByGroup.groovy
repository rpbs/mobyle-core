import jalview.analysis.*;
import jalview.datamodel.*;
import jalview.gui.AlignFrame;
import jalview.gui.AlignViewport;

def af = Jalview.getAlignframes();

def todie = "PDBFile"; // about to delete this type of group
for (ala in af)
{
	def al = ala.viewport.alignment.getDataset();
	if (al!=null)
	{
		SequenceI[] seqs = al.getSequencesArray();
		for (sq in seqs)
		{
			if (sq!=null) {
				SequenceFeature[] sf = sq.getSequenceFeatures();
				for (sfpos in sf)
				{
				    if (sfpos!=null && sfpos.getFeatureGroup().equals(todie))
					{
						sq.deleteFeature(sfpos);
					}
				}
			}
		}
	}
}
	


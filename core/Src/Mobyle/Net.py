########################################################################################
#                                                                                      #
#   Author: Bertrand Neron,                                                            #
#   Organization:'Biological Software and Databases' Group, Institut Pasteur, Paris.   #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.        #
#                                                                                      #
########################################################################################


import smtplib
import mimetypes
from email import Encoders
from email.MIMEBase import MIMEBase
from email.MIMEMultipart import MIMEMultipart
from email.MIMEImage import MIMEImage
from email.MIMEText import MIMEText
import re
import os 
import types

from Mobyle.MobyleError import MobyleError , UserValueError , EmailError , TooBigError

import Local.black_list
import Local.Policy
import Local.mailTemplate
import Local.ticketTemplate

from logging import getLogger
n_log = getLogger( __name__ )

from Mobyle.ConfigManager import Config
cfg = Config()

from urlparse import urljoin
from time import sleep

def checkHost( ):

    try:
        userIP = os.environ[ 'REMOTE_ADDR' ]
    except KeyError:
        #MobyleJob is executed from commandline
        return True

    #rewriting the blacklist in a regexp
    pattern = '|'.join( Local.black_list.host )
    pattern = pattern.replace('.' , '\.' )
    pattern = pattern.replace('*' , '.*')
    pattern = "^(%s)$" % pattern
    auto = re.compile( pattern )

    method= 'host'
    if auto.search( userIP ):
        try:
            userMsg = Local.Policy.emailUserMessage( method )
        except:
            userMsg = "you are not allowed to run on this server for now"
        return NetResponse( False, 'IP address is in black list', userMsg, method )
    else:
        return NetResponse( True , '' , '' , method )


class EmailAddress:
        
    INVALID = 0
    VALID = 1
    CONTINUE = 2
    
    def __init__( self , addr ):
        """
        @param addr: the emails addresses
        @type addr: string or list of strings
        """
        if not addr:
            raise MobyleError , 'addr must not be empty'
        if isinstance( addr , types.StringTypes ):
            self.addr = [ addr.strip() ]
        elif isinstance( addr , ( types.ListType , types.TupleType ) ):
            self.addr = addr 
        else:
            raise MobyleError , " addr must be a string or a list of strings : "+ str( addr )
        
        self._methods = [ self._checkSyntax ,
                          self._checkBlackList ,
                          self._checkLocalRules ]      
        
        if cfg.dnsResolver():
            self._methods.append( self._checkDns )
        
        self._message = ''        
    
    def __str__(self):
        return ','.join( self.addr )
    
    def getAddr(self):
        return [ addr for addr in self.addr ]
    
    
    def check( self ):
        """
        check if the addresses are valid:
         right syntax, 
         not in black list, 
         according to the local rules 
         and the domain have a mx fields)
        @return: True if the addresses pass the controls, False otherwise
        @rtype: boolean  
        """
        self._message = ''
        for oneAddr in self.addr:
            for method in self._methods:
                rep = method( oneAddr )
                if rep.status == self.CONTINUE :
                    continue
                elif rep.status == self.VALID or rep.status == self.INVALID:
                    return rep
                else:
                    raise MobyleError , method.__name__+ " return an invalid response :"+ str(rep)
        return NetResponse( self.VALID , '' , '' , '' )


    def getMessage( self ):
        return self._message
            
    def _checkSyntax( self , addr ):
        method =  'syntax'
        email_pat = re.compile( "^[a-z0-9\-\.+_]+\@([a-z0-9\-]+\.)+([a-z]){2,4}$" , re.IGNORECASE )
        match = re.match( email_pat , addr )

        if match is None:
            try:
                userMsg = Local.Policy.emailUserMessage( method )
            except:
                userMsg = "you are not allowed to run on this server for now"
            return NetResponse(self.INVALID , "invalid syntax for email address" , userMsg,  method )
        else:
            return NetResponse(self.CONTINUE , '' , '' ,  method )

    def checkBlackList( self ):
        for oneAddr in self.addr:
            rep = self._checkBlackList( oneAddr )
            if rep.status == self.CONTINUE :
                continue
            elif rep.status == self.VALID or rep.status == self.INVALID:
                return rep
        return NetResponse( self.VALID , '' , '' , '' )
        
    def _checkBlackList( self , addr ):
        pattern = '|'.join( Local.black_list.users )
        pattern = pattern.replace('.' , '\.' )
        pattern = pattern.replace('*' , '.*')
        pattern = "^(%s)$" % pattern
        auto = re.compile( pattern )
        method = 'blackList'
        if auto.search( addr ):
            try:
                userMsg = Local.Policy.emailUserMessage( method )
            except:
                userMsg = "you are not allowed to run on this server for now"
            return NetResponse(self.INVALID , "email is in black_list" , userMsg , method )
        else:
            return NetResponse(self.CONTINUE , '' , '' , method )


    def _checkLocalRules( self, addr  ):
        #tester si le module existe ? existe toujours meme si vide ?
        rep = Local.Policy.emailCheck( email = addr )
        message = ''
        userMsg = ''
        method = 'LocalRules'
        if rep == self.INVALID:
            message = "email is rejected by our local policy"
            try:
                userMsg = Local.Policy.emailUserMessage( method )
            except:
                userMsg = "you are not allowed to run on this server for now"
        return NetResponse( rep , message , userMsg , method)


    def _checkDns( self , addr ):
        import dns.resolver
        method = 'dns'
        try:
            userMsg = Local.Policy.emailUserMessage( method )
        except:
            userMsg = "you are not allowed to run on this server for now"
        user , domainName  = addr.split('@')
        try:
            answers = dns.resolver.query( domainName , 'MX')
        except dns.resolver.NXDOMAIN , err :
            self._message = "unknown name domain"
            return NetResponse( self.INVALID , "unknown name domain" , userMsg , method )
        except dns.resolver.NoAnswer ,err :
            try:
                answers = dns.resolver.query( domainName , 'A')
            except:
                return NetResponse( self.INVALID , "no mail server" , userMsg , method )
            return NetResponse( self.CONTINUE , '' , '' , method )
        except (dns.name.EmptyLabel, dns.resolver.NoNameservers):
            return NetResponse( self.INVALID , "no domain name server" , userMsg , method )
        except dns.exception.Timeout:
            return NetResponse( self.CONTINUE, "dns timeout" , userMsg , method )
        except Exception, err:
            msg = "unexpected error in  Email._checkDns : "+ str( err )
            n_log.critical( msg, exc_info = True )
            return NetResponse( self.INVALID , msg , userMsg , method )
        return NetResponse( self.CONTINUE , '' , '' , method )
            
class NetResponse:
    
    def __init__(self, status , message , user_message , method ):
        """ """
        self.status = status
        """Internal Mobyle Message"""
        self.message = message
        """User Message"""
        self.user_message = user_message
        """the name of the method"""
        self.method = method
    

        
class Email: 
    
    def __init__( self , To , cc = None): 
        """
        @param To: the recipients addresses
        @type To: EmailAddress instance
        @param cc: the emails adresses in copy of this mail
        @type cc: EmailAddress instance
        """   
        self.To = To
        self.cc = cc
        self.mailhost = cfg.mailhost()
        self.headers = None
        self.body = None
        
    def getBody(self):
        return self.body
    
    def getHeaders(self):
        return self.headers
    
    def send( self , templateName , dict ,  files= None  ):
        """
        send an email to the Email.To recipients, using the template to build email body.
        @param template: the template of the mail. see Local/mailTemplate.py
        @type template: string 
        @param dict: the dictionnary used to expend the template
        @type dict: dictionnary
        @param files: the list of file names to attach to this email
        @type files: list of strings 
        """
        try:
            template = getattr( Local.mailTemplate , templateName )
        except AttributeError ,err:
            msg = "error during template %s loading: err" %( templateName ,err )
            n_log.critical( msg )
            raise MobyleError , err 
        try:
            mail = template % dict
        except ( TypeError , KeyError ) , err:
            msg = "error during template %s expanding: %s. This mail sending is aborted" %( templateName , err )
            n_log.critical( msg )
            raise MobyleError , msg 
        if not mail and not files:
            errMsg = "no msg and no files for template %s send email aborted" % templateName
            n_log.warning( errMsg )
            return None
        
        mailAttr , msg = self._parse( mail )
        if files:
            emailBody = MIMEMultipart()
        else:
            emailBody = MIMEText( msg , 'plain' , 'utf-8')
        
        mailAttr[ 'To' ] = str( self.To )
        recipients = self.To.getAddr()
        if self.cc:
            recipients.extend( self.cc.getAddr() )
            emailBody[ 'Cc' ] = str( self.cc )
            
        for attr in mailAttr.keys():
            if attr == 'Reply-To':
                emailBody[ attr ] = ', '.join( mailAttr[ attr ] )
            elif attr == 'Cc' or attr == 'Bcc' :
                recipients.extend( mailAttr[ attr ] )
                emailBody[ attr ] = ', '.join( mailAttr[ attr ] )
            else:
                emailBody[ attr ] =  mailAttr[ attr ] 
        try:
            try:
                s = smtplib.SMTP( self.mailhost, timeout= 30 )
            except TypeError:
                # timeout was added in python 2.6
                s = smtplib.SMTP( self.mailhost )
        except Exception, err:
            #except smtplib.SMTPException , err:
            n_log.error( "can't connect to mailhost \"%s\"( check Local.Config.Config.py ): %s" %( self.mailhost , err ) )
            raise EmailError , err
        
        if files:
            emailBody.preamble = 'You will not see this in a MIME-aware mail reader.\n'
            # To guarantee the message ends with a newline
            emailBody.epilogue = ''            
            if msg :
                msg = MIMEText( msg  , 'plain' , 'utf-8' )
                emailBody.attach( msg )

            for filename in files:
                if not os.path.isfile( filename ):
                    continue
    
                # Guess the content type based on the file's extension.  Encoding
                # will be ignored, although we should check for simple things like
                # gzip'd or compressed files.
    
                ctype, encoding = mimetypes.guess_type( filename )
                if ctype is None or encoding is not None:
                    # No guess could be made, or the file is encoded (compressed), so
                    # use a generic bag-of-bits type.
                    ctype = 'application/octet-stream'
    
                maintype, subtype = ctype.split( '/' , 1 )
                if maintype == 'text':
                    fp = open( filename , 'r')
                    # Note: we should handle calculating the charset
                    msg = MIMEText( fp.read() , _subtype = subtype )
                    fp.close()
                elif maintype == 'image':
                    fp = open( filename , 'rb' )
                    msg = MIMEImage( fp.read() , _subtype = subtype )
                    fp.close()
                else:
                    if filename == 'index.xml':
                        fp = self._indexPostProcess()
                    else:
                        fp = open( filename , 'rb' )
                        
                    msg = MIMEBase( maintype , subtype )
                    msg.set_payload( fp.read() )
                    fp.close()
                    # Encode the payload using Base64
                    Encoders.encode_base64( msg )
    
                # Set the filename parameter
                msg.add_header( 'Content-Disposition' , 'attachment', filename = os.path.basename( filename) )
                emailBody.attach( msg )
        
        try:
            s.sendmail( mailAttr[ 'From' ] , recipients , emailBody.as_string() )
            s.quit()
        except smtplib.SMTPSenderRefused, err:
            if err.smtp_code == 552 :
                raise TooBigError , str( err )
            else:
                raise EmailError , err
        except smtplib.SMTPException , err:
            n_log.error( "can't send email : %s" % err )
            raise EmailError , err
                
        except Exception , err:    
            #except smtplib.SMTPException , err:
            n_log.error( "can't send email : %s" % err , exc_info = True)
            n_log.error( str( recipients ) )
            raise EmailError , err                
            
    def _parse(self , mail ):
        headers = {}
        mail = mail.split( '\n' )
        iterator = iter( mail )
        begin = False

        for line in iterator:
            if not (begin or line):
                begin = True
                continue

            splitedLine = line.split(':')
            fields = splitedLine[0].strip()
            value = ':'.join( splitedLine[1:] )
            if fields == 'From':
                value = value.split( ',' )
                try:
                    value = value[0].strip()
                except IndexError :
                    raise MobyleError, '"From:" field cannot be empty '
                if not value:
                    raise MobyleError, '"From:" field cannot be empty '
            elif fields in ( 'Reply-To' , 'Cc' , 'Bcc' ) :
                value = value.split( ',' )
                value = [ val.strip() for val in value if val.strip() ]
                if not value:
                    continue
            elif not fields:
                body = '\n'.join( iterator )
                break
            else:
                value = value.strip()
                if not value:
                    continue
            headers[ fields ] = value
        self.headers = headers
        self.body = body
        return   headers , body

class Ticket: 

    def __init__( self, assignTo ):

        import requests
        self._requests = requests
        self.assignTo = assignTo
        self.tickets_endpoint = cfg.tickets_endpoint()
        self.tickets_recipient = cfg.tickets_recipient()
        self.headers = { "Api-Key" : cfg.tickets_apikey(),
                         "Api-Username" : cfg.tickets_apiuser() }

    def send( self , templateName , dict ):

        try:
            template = getattr( Local.ticketTemplate , templateName )
        except AttributeError ,err:
            msg = "error during template %s loading: err" % ( templateName ,err )
            n_log.critical( msg )
            raise MobyleError , err 
        try:
            email = template % dict
        except ( TypeError , KeyError ) , err:
            msg = "error during template %s expanding: %s. This ticket sending is aborted" %( templateName , err )
            n_log.critical( msg )
            raise MobyleError , msg 

        # create topic

        url = urljoin(self.tickets_endpoint, "admin/email/handle_mail")
        data = { "email" : email }

        try:
            r = self._requests.post(url, headers = self.headers, data = data)
        except Exception , err:
            n_log.error( "can't post ticket : %s" % err , exc_info = True)
            raise EmailError , err

        staged_user = str(dict["USER"]).split("@")[0]

        sleep(1)

        topic_id = self._getTopicId(staged_user)

        if topic_id and self.assignTo:
            self._assignTopic(topic_id, self.assignTo)

    def _getTopicId( self, user ):
        """
        retrieve topic id
        """
        url = urljoin(self.tickets_endpoint, "topics/created-by/%s.json" % user)

        try:
            r = self._requests.get(url, headers = self.headers)
            return r.json()["topic_list"]["topics"][0]["id"]
        except Exception , err:
            n_log.error( "can't retrieve ticket id : %s" % err , exc_info = True)
            return None

    def _assignTopic( self, topicId, assignTo):
        """
        assign topic
        """
        url = urljoin(self.tickets_endpoint, "assign/assign")
        data = { "username" : assignTo,
                 "topic_id" : topicId }

        try:
            r = self._requests.put(url, headers = self.headers, data = data)
        except Exception , err:
            n_log.error( "can't assign ticket : %s" % err , exc_info = True)

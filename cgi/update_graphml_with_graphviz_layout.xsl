<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:param name="cssPath" />

	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="data[ @key = 'xy']">
		<xsl:variable name="node_id" select="../@id" />
		<xsl:variable name="coords"
			select="document(concat('file://',$cssPath))/map/area[@title = $node_id ]/@coords" />
		<xsl:variable name="x" select="substring-before($coords,',')" />
		<xsl:variable name="tmp" select="substring-after($coords,',')" />
		<xsl:variable name="y" select="substring-before($tmp,',')" />
		<xsl:element name="data">
			<xsl:attribute name="key">
                                <xsl:value-of select="'xy'" />
                            </xsl:attribute>
			<xsl:value-of select="concat($x,',',$y+50)" />
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>

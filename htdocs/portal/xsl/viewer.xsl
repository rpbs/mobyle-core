<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  
  <xsl:output method="html" indent="yes" />	

  <xsl:include href="ident.xsl" />
  
  <xsl:param name="jobUrl" />

  <xsl:param name="previewDataLimit">1048576</xsl:param>  

  <xsl:param name="htdocs">/portal/</xsl:param>
  
  <xsl:param name="codebase">/data/</xsl:param>  

  <xsl:variable name="job" select="document($jobUrl)"/>
    
  <xsl:variable name="server" select="''" />  

  <xsl:template match="*">
    <xsl:element name="{local-name()}">
      <!-- go process attributes and children -->
      <xsl:apply-templates select="@*|node()"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="/viewer">
    <html>
      <head>
        <title>Mobyle - <xsl:value-of select="head/name/text()"/></title>
        <link rel="stylesheet" href="{$htdocs}css/mobyle.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="{$htdocs}css/local.css" type="text/css" media="screen" />
      </head>
      <body>
          <div class="viewerRelContainer">
            <div class="header">
              <xsl:apply-templates select="head" mode="serviceHeader"/>
              <xsl:apply-templates select="head/doc/comment" mode="ajaxTarget"/>
            </div>      
            <xsl:if test="$jobUrl">
              <div class="data_view">
                <xsl:apply-templates select="head/interface[@type='viewer']"/>          
              </div>
            </xsl:if>
            <div class="footer">
              <xsl:apply-templates select="//parameter[@isout]" />        
              <xsl:apply-templates select="head" mode="serviceFooter"/>
            </div>
          </div>
      </body>
      </html>
  </xsl:template>

  <xsl:template match="head/interface[@type='viewer']">
        <xsl:apply-templates select="*"/>        
  </xsl:template>    

  <xsl:template match="parameter[@isout]">
    <div style="display: none;" data-viewerport="{name/text()}" data-parametername="name/text()" data-datatype="{type/datatype/class/text()}" 
      data-datatype-superclass="{type/datatype/superclass/text()}" data-format="{type/dataFormat/text()}" data-inputmodes="result" data-filename="{name/text()}"
      data-portcode="{format/code/text()}" data-base64encoded="{format/@base64encoded}" data-extension="{format/@extension}" data-prompt="{prompt/text()}">
      <xsl:attribute name="data-biotype">
        <xsl:for-each select="type/biotype">
          <xsl:value-of select="text()" /><xsl:text> </xsl:text>
        </xsl:for-each>
      </xsl:attribute>
<!--
      <button>
        <xsl:attribute name="data-test">
          <xsl:value-of select="format/code/text()"/>
        </xsl:attribute>
        <xsl:attribute name="onclick">
          if(portal){bookmark_upload(<xsl:value-of select="format/code/text()"/>,$("<xsl:value-of select="name/text()"/>.username").value,'<xsl:value-of select="type/datatype/class/text()"/>','<xsl:value-of select="type/datatype/superclass/text()"/>',
          '<xsl:value-of select="type/dataFormat/text()"/>','<xsl:value-of select="type/biotype/text()"/>',$("viewerFlash"),<xsl:value-of select="format/@base64encoded"/>);};
        </xsl:attribute>
        bookmark 
        <xsl:value-of select="prompt"/>
      </button>
      <xsl:text> as </xsl:text>
      <input type="text" value="{prompt}" size="8" name="{name}.username" id="{name}.username"/>
-->
    </div>
  </xsl:template>
    
  <xsl:template match="xhtml:*[@data-paragraphname]">
    <!-- do not display a paragraph unless there are data to be displayed -->
    <xsl:if test="$job/jobState/data//*[name/text()=current()//@data-parametername]">
      <xsl:element name="{local-name(.)}">
        <xsl:apply-templates select="@*|node()" />
      </xsl:element>
    </xsl:if>
  </xsl:template>

 
  <xsl:template match="@*">
    <xsl:choose>
      <xsl:when test="../@data-parametername and contains(.,'data-url')">
        <xsl:variable name='parametername' select="../@data-parametername"/>
        <xsl:variable name="data-url" select="$job/jobState/data[parameter/name/text()=$parametername]/file/text()"/>            
        <xsl:attribute name="{name()}" namespace="{namespace-uri()}"><xsl:value-of select="substring-before(.,'data-url')"/><xsl:value-of select="$data-url"/><xsl:value-of select="substring-after(.,'data-url')"/></xsl:attribute>        
      </xsl:when>
      <xsl:when test="contains(.,'viewer-codebase')">
        <xsl:attribute name="{name()}" namespace="{namespace-uri()}"><xsl:value-of select="substring-before(.,'viewer-codebase')"/><xsl:value-of select="concat($codebase,'services/servers/local/viewers/',/*/head/name/text())"/><xsl:value-of select="substring-after(.,'viewer-codebase')"/></xsl:attribute>        
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
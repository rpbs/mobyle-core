<?xml version="1.0" encoding="utf-8"?>
<!-- 
	job_graph.xsl stylesheet
	Authors: Hervé Ménager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" 
	xmlns:xhtml="http://www.w3.org/1999/xhtml" 
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:xlink="http://www.w3.org/1999/xlink">

	<xsl:include href="ident.xsl" />

	<xsl:param name="jobPID" />	

	<xsl:template match="processing-instruction()">
	</xsl:template>
	
	<xsl:template match="/jobState">
		<xsl:apply-templates select="workflow/head/interface[@type='graph']/svg:svg"/>
	</xsl:template>

	<xsl:template match="svg:g[svg:title/text()=/jobState/jobLink/@taskRef]">
		<xsl:variable name="subJobId" select="/jobState/jobLink[@taskRef=current()/svg:title/text()]/@jobId" />
		<xsl:variable name="subJobDoc" select="document(concat($subJobId,'/index.xml'))" />
		<xsl:variable name="subJobServer" select="/jobState/workflow/flow/task[@id=current()/svg:title/text()]/@server" />		
		<xsl:variable name="subTaskPIDSuff" select="$subJobDoc/jobState/*/head/name/text()" />
		<xsl:variable name="subTaskPID">
			<xsl:if test="$subJobServer!=''"><xsl:value-of select="$subJobServer"/>.</xsl:if>
			<xsl:value-of select="$subTaskPIDSuff"/>
		</xsl:variable>
		<xsl:variable name="subJobPID" select="concat($subTaskPID,'.',substring-after($subJobId,concat($subTaskPIDSuff,'/')))" />
		<svg:a target="_top" xlink:href="#jobs::{$jobPID}::{$subJobPID}">
			<xsl:variable name="statusValue">
				<xsl:choose>
					<xsl:when test="$subJobDoc/jobState/status">
						<xsl:value-of select="/jobState/status/value/text()" />
					</xsl:when>      
					<xsl:otherwise>
						<xsl:value-of select="document(concat($subJobId,'/mobyle_status.xml'))/status/value/text()" />        
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>			
			<xsl:attribute name="class">
				<xsl:text>jobstatus </xsl:text>
				<xsl:value-of select="$statusValue" />
			</xsl:attribute>
			<xsl:call-template name="nodeCopy" />
		</svg:a>
	</xsl:template>

</xsl:stylesheet>
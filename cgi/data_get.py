#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from  Mobyle.Classes.DataType import DataTypeFactory

def process( self ):
    if(not(self.request.has_key('id'))):
        self.jsonMap['errormsg'] = "Please specify the id of the data that has to be downloaded."
    else:
        id = self.request.getfirst('id')
        # mobyle type determines wether we send back the data or not (if binary)
        d = self.session.getData(id)
        mt = d['Type']
        df = DataTypeFactory()
        contentData= self.session.getContentData(id)
        self.jsonMap['userName'] = d['userName']
        self.jsonMap['safeFileName'] = d['dataName']
        if df.issubclass(mt.getDataType(),"Binary"):
            self.jsonMap['content'] = d['dataBegin']          
        else:
            self.jsonMap['headFlag'] = contentData[0]
            if(self.jsonMap['headFlag']=="HEAD"):
                self.jsonMap['content'] = contentData[1][0:2000]
            else:
                self.jsonMap['content'] = contentData[1]
              
if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)
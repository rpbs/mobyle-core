#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.Registry import registry

def process(self):
    for service in registry.serversByName['local'].programs+registry.serversByName['local'].workflows:
        self.jsonMap[service.name] = {
                                      'url': service.url,
                                      'name': service.name,
                                      'type': service.type,
                                      'disabled': service.disabled,
                                      'authorized': service.authorized,
                                      'exported': service.isExported()
                                      }
if __name__ == "__main__":
    il = mb_cgi.JSONCGI(processFunction=process)
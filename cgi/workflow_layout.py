#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.Registry import registry

def process(self):
    self.mime_type = 'image/svg+xml'
    if self.request.getfirst('id'):
        def f(x): return x and x != '.'
        id = filter(f,self.request.getfirst('id').partition('.'))
        try:
            if len(id)==1:
                self.service = registry.serversByName['local'].programsByName[id[0]]    
            else:
                self.service = registry.serversByName[id[0]].programsByName[id[1]]
        except KeyError, e:
            if len(id)==1:
                self.service = registry.serversByName['local'].workflowsByName[id[0]]            
            else:
                self.service = registry.serversByName[id[0]].workflowsByName[id[1]]            
    self.xmlUrl = self.service.path
    self.xslPipe = [
                    (self.cfg.portal_path()+"/xsl/form_graph.xsl",{})
                    ]

if __name__ == "__main__":
    il = mb_cgi.XSLCGI(processFunction=process,useSession=True)
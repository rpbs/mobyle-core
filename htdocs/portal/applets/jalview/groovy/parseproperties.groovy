import jalview.analysis.*;
import jalview.datamodel.*;
import jalview.gui.AlignFrame;
import jalview.gui.AlignViewport;

def af = Jalview.getAlignframes();
def al = af[0].viewport.alignment;
ParseProperties pp = new ParseProperties(al);
pp.getScoresFromDescription("Score", "ScanPS Raw Score", "([-0-9.+]+)");
def sqs = al.getSequenceAt(0);
def annots = sqs.getAnnotation();



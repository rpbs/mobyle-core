#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
from Mobyle.MobyleError import UserValueError,EmailError
from Mobyle.Registry import registry
from Mobyle.Utils import emailHelpRequest

def process( self ):
    try:
        self.jsonMap['ok'] = str(False)
        self.jsonMap['msg'] = emailHelpRequest(self.cfg, self.request.getfirst('helpEmail',None), registry, self.request.getfirst('id',None), self.request.getfirst('helpMessage',None),self.session, self.request.getfirst('param',None), self.request.getfirst('message',None)) 
        self.jsonMap['ok'] = str(True)
    except UserValueError, ue:
        self.jsonMap['errormsg'] = ue.message
        return
    except EmailError:
        self.jsonMap['errormsg'] = "please provide a valid email adress"
        return
        
if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)
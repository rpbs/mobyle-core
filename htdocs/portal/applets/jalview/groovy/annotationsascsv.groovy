// do something groovy in jalview
print "Hello World.\n";
def alf = Jalview.getAlignframes();
for (ala in alf)
{
	def alignment = ala.viewport.alignment;
	// ala is an jalview.gui.AlignFrame object 
	print new jalview.io.AnnotationFile().printCSVAnnotations(alignment.getAlignmentAnnotation())+"\n";
}
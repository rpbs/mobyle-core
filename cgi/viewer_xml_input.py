#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi

def process(self):
    template = """<jobState> 
  <data>
  %s
  </data>
</jobState>
    """
    paramTemplate = """
  <parameter> 
    <name>%s</name> 
  </parameter> 
  <file>%s</file> 
    """
    self.xmltext = ''
    if id:
        self.parameters = self.request.getlist('parameter')
        for param in self.parameters:
            name, value = param.split('|')
            if not(value.startswith(self.cfg.root_url())):
                value = "%s/file_load.py?url=%s" %( self.cfg.cgi_url() , value)
            self.xmltext += paramTemplate % (name, value)
        self.xmltext = template % (self.xmltext)

if __name__ == "__main__":
    il = mb_cgi.XMLCGI(processFunction=process)
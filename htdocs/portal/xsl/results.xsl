<?xml version="1.0" encoding="utf-8"?>
<!-- 
	form_parameters.xsl stylesheet
	Authors: Hervé Ménager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
	
	<xsl:include href="ident.xsl" />

	<xsl:param name="jobUrl" />
	
	<xsl:template match="parameters">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>	

	<xsl:template match="parameter[(@isout or @isstdout) and count(ancestor-or-self::*/interface[@type='job_output'])=0]">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<interface generated="true" type="job_output">
				<div xmlns="http://www.w3.org/1999/xhtml" data-parametername="{name/text()}">
				</div>
			</interface>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="parameter[not(@isout or @isstdout) and count(ancestor-or-self::*/interface[@type='job_input'])=0]">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<interface generated="true" type="job_input">
				<div xmlns="http://www.w3.org/1999/xhtml" data-parametername="{name/text()}">
				</div>
			</interface>
		</xsl:copy>
	</xsl:template>		

</xsl:stylesheet>

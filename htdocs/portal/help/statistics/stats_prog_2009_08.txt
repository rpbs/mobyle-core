# -- 
# AccessLogInfos Report v0.3
# -- 
# Mobyle  Log file: ['/var/log/Mobyle/access_log', '/var/log/Mobyle/bkp/access_log']
# Period 2009/08/01-2009/08/31
HCA                 :  684 jobs  50.52%  250 unique IPs
PEP-FOLD            :  196 jobs  14.48%   48 unique IPs
psipred             :  172 jobs  12.70%   14 unique IPs
admetox             :   76 jobs   5.61%   22 unique IPs
wwLig-CSRre         :   36 jobs   2.66%   16 unique IPs
OpenBabel           :   34 jobs   2.51%   15 unique IPs
PASS                :   13 jobs   0.96%    9 unique IPs
Dihedrals           :   12 jobs   0.89%    4 unique IPs
DihedralEdit        :    8 jobs   0.59%    2 unique IPs
fpocket             :    8 jobs   0.59%    5 unique IPs
PCE-pKa             :    7 jobs   0.52%    4 unique IPs
SSpro               :    7 jobs   0.52%    6 unique IPs
ASA                 :    6 jobs   0.44%    4 unique IPs
ProbCons            :    6 jobs   0.44%    3 unique IPs
water               :    6 jobs   0.44%    1 unique IPs
Ali2Modeller        :    5 jobs   0.37%    1 unique IPs
CS-Blast            :    5 jobs   0.37%    2 unique IPs
CysState            :    5 jobs   0.37%    2 unique IPs
Frog                :    5 jobs   0.37%    3 unique IPs
PoPMuSic            :    5 jobs   0.37%    4 unique IPs
matcher             :    5 jobs   0.37%    2 unique IPs
PDBBlast2           :    4 jobs   0.30%    2 unique IPs
coudes              :    4 jobs   0.30%    3 unique IPs
garnier             :    4 jobs   0.30%    1 unique IPs
extractTurn         :    3 jobs   0.22%    3 unique IPs
iSuperpose          :    3 jobs   0.22%    2 unique IPs
jmol_example        :    3 jobs   0.22%    2 unique IPs
stride              :    3 jobs   0.22%    3 unique IPs
H-bonds             :    2 jobs   0.15%    2 unique IPs
LogP                :    2 jobs   0.15%    2 unique IPs
ProFitv2_6          :    2 jobs   0.15%    2 unique IPs
helixturnhelix      :    2 jobs   0.15%    2 unique IPs
needle              :    2 jobs   0.15%    1 unique IPs
AutomatP            :    1 jobs   0.07%    1 unique IPs
JME                 :    1 jobs   0.07%    1 unique IPs
MIR                 :    1 jobs   0.07%    1 unique IPs
PCE-pot             :    1 jobs   0.07%    1 unique IPs
SCSubstitute        :    1 jobs   0.07%    1 unique IPs
antigenic           :    1 jobs   0.07%    1 unique IPs
avp                 :    1 jobs   0.07%    1 unique IPs
basicbuilder        :    1 jobs   0.07%    1 unique IPs
coderet             :    1 jobs   0.07%    1 unique IPs
dotmatcher          :    1 jobs   0.07%    1 unique IPs
hmoment             :    1 jobs   0.07%    1 unique IPs
iep                 :    1 jobs   0.07%    1 unique IPs
neighbor            :    1 jobs   0.07%    1 unique IPs
p-sea               :    1 jobs   0.07%    1 unique IPs
pepnet              :    1 jobs   0.07%    1 unique IPs
restrict            :    1 jobs   0.07%    1 unique IPs
stretcher           :    1 jobs   0.07%    1 unique IPs
supermatcher        :    1 jobs   0.07%    1 unique IPs
wordfinder          :    1 jobs   0.07%    1 unique IPs
# --------------------------------------------------------------------
Total               : 1354 jobs (461 unique IPs)

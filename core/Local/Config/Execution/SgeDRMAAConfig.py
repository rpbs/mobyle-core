########################################################################################
#                                                                                      #
#   Author: Bertrand Neron,                                                            #
#   Organization:'Biological Software and Databases' Group, Institut Pasteur, Paris.   #  
#   Distributed under GPLv2 Licence. Please refer to the COPYING.LIB document.         #
#                                                                                      #
########################################################################################

import logging 
_log = logging.getLogger(__name__)
from Mobyle.MobyleError import MobyleError
from DRMAAConfig import DRMAAConfig
         
class SgeDRMAAConfig( DRMAAConfig ):
    
        def __init__( self , drmaa_library_path , root = None , cell = None , nativeSpecification = '' ):
            if root is None or cell is None:
                msg = "root and cell must be specified for SgeDRMAAConfig instanciation in Config.EXECUTION_SYSTEM_ALIAS"
                _log.critical( msg )
                raise MobyleError( msg )
            super( SgeDRMAAConfig , self ).__init__( drmaa_library_path , nativeSpecification = nativeSpecification )
            # sge drmaa does not support several server at same time
            # so contactString must remain empty
            # but SGE_CELL must be defined
            self.contactString = ''
            self.cell = cell
            self.root = root
            

        
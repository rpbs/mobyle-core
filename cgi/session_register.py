#!/usr/bin/python
#############################################################
#                                                           #
#   Author: Herve Menager                                   #
#   Organization:'Biological Software and Databases' Group, #
#                Institut Pasteur, Paris.                   #
#   Distributed under GPLv2 Licence. Please refer to the    #
#   COPYING.LIB document.                                   #
#                                                           #
#############################################################
import mb_cgi
import Mobyle.Net
from Mobyle.MobyleError import SessionError

def process(self):
    try:
        assert self.request.getfirst('email'), "Please provide an e-mail"
        assert self.request.getfirst('password'), "Please provide a password"
        assert self.request.getfirst('password')==self.request.getfirst('passwordCheck'), "The password values you entered need to be identical"
    except AssertionError, ae:
        self.error_msg = str(ae)
        return
    try:
        email = Mobyle.Net.EmailAddress( self.request.getfirst('email') )
        self.anonymousSession = self.session
        self.session = self.sessionFactory.createAuthenticatedSession( email , self.request.getfirst('password') )
        if self.anonymousSession:
            self.session.mergeWith(self.anonymousSession)
        self.sessionKey = self.session.getKey()
        self.read_session()
        self.jsonMap['ok'] = str(True)
    except SessionError, e:
        self.jsonMap['ok'] = str(False)
        self.jsonMap['errormsg'] = str(e)
      
if __name__ == "__main__":
    mb_cgi.JSONCGI(processFunction=process,useSession=True)
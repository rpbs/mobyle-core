import jalview.analysis.*;
import jalview.datamodel.*;
import jalview.gui.AlignFrame;
import jalview.gui.AlignViewport;

def af = Jalview.getAlignframes();

for (ala in af)
{
	def al = ala.viewport.alignment;
	if (al!=null && al.getCodonFrames()!=null)
	{
		al.getCodonFrames().each { 
			it.getdnaToProt().each {
				it2 -> println it2.fromShifts+"\n"
			}
		}
	}
}
	

<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
	form_parameters.xsl stylesheet
	Authors: Herv� M�nager
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
	
	<xsl:include href="ident.xsl" />
	
	<xsl:template match="parameter[not(@isout) and not(@isstdout) and not(@ishidden) and not(ancestor-or-self::*[interface/@type='form'])]">
		<xsl:copy>
			<xsl:variable name ="dataType">
				<xsl:choose>
					<xsl:when test="type/datatype/superclass"><xsl:value-of select="type/datatype/superclass"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="type/datatype/class"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:apply-templates select="@*|node()" />
			<interface type='form' generated="true">
				<xsl:choose>
					<xsl:when test="($dataType = 'Choice') or ($dataType = 'MultipleChoice') or ($dataType = 'Boolean') or ($dataType = 'Integer') or ($dataType = 'Float') or ($dataType = 'String') or ($dataType = 'Filename')">
						<xhtml:label>
							<xsl:value-of select="prompt" />
							<xsl:apply-templates select="." mode="controlChoice"/>
						</xhtml:label>						
					</xsl:when>
					<xsl:otherwise>
						<xhtml:fieldset>
							<xhtml:legend><xsl:value-of select="prompt" /></xhtml:legend>
							<xsl:apply-templates select="." mode="controlChoice"/>
						</xhtml:fieldset>												
					</xsl:otherwise>
				</xsl:choose>
			</interface>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="parameter/name" mode="controlChoice">
		<xsl:attribute name="name"><xsl:value-of select="text()"/></xsl:attribute>
	</xsl:template>	

	<xsl:template match="parameter" mode="controlChoice">
		<!-- Textarea or Input controls -->
		<xsl:variable name ="dataType">
			<xsl:choose>
				<xsl:when test="type/datatype/superclass"><xsl:value-of select="type/datatype/superclass"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="type/datatype/class"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="inputElement">
			<xsl:choose>
				<xsl:when test="($dataType = 'Integer') or ($dataType = 'Float') or ($dataType = 'String') or ($dataType = 'Filename') or ($dataType = 'Binary')">input</xsl:when>
				<xsl:otherwise>textarea</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="inputType">
			<xsl:choose>
				<xsl:when test="($dataType = 'Integer') or ($dataType = 'Float') or ($dataType = 'String') or ($dataType = 'Filename')">text</xsl:when>
				<xsl:when test="$dataType = 'Binary'">file</xsl:when>
			</xsl:choose>			
		</xsl:variable>
		<xsl:element name="xhtml:{$inputElement}">
			<xsl:apply-templates select="name" mode="controlChoice"/>
			<xsl:if test="$inputElement='input'"><xsl:attribute name="type"><xsl:value-of select="$inputType"/></xsl:attribute></xsl:if>
			<xsl:if test="$inputElement='textarea'"><xsl:attribute name="cols">60</xsl:attribute><xsl:attribute name="rows">7</xsl:attribute></xsl:if>
			<xsl:choose>
				<!-- Default value -->
				<xsl:when test="$inputElement='input' and $inputType!='checkbox'"><xsl:attribute name="value"><xsl:value-of select="vdef/value/text()"/></xsl:attribute></xsl:when>
				<xsl:when test="$inputElement='input' and $inputType='checkbox' and vdef/value/text()='1'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:when>
				<xsl:when test="$inputElement='textarea'">
					<xsl:choose>
						<xsl:when test="vdef and vdef/value/text()">
							<xsl:value-of select="vdef/value/text()"/>
						</xsl:when>
						<xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
					</xsl:choose>					
				</xsl:when>
			</xsl:choose>
		</xsl:element>
	</xsl:template>	

	<xsl:template match="parameter[*[substring(local-name(.),2,4)='list'] or (type/datatype/superclass/text()='Boolean' or type/datatype/class/text()='Boolean')]" mode="controlChoice">
		<!-- Select control for choices and booleans -->
		<xhtml:select>
			<xsl:if test="type/datatype/class='MultipleChoice'">
				<xsl:attribute name="multiple">multiple</xsl:attribute>
			</xsl:if>			
			<xsl:apply-templates select="name" mode="controlChoice"/>
			<xsl:apply-templates select="*[substring(local-name(.),2,4)='list']//value" mode="controlChoice"/>	
			<xsl:if test="type/datatype/superclass/text()='Boolean' or type/datatype/class/text()='Boolean'">
				<xsl:call-template name="selectOption">
					<xsl:with-param name="value" select="1" />
					<xsl:with-param name="label" >Yes</xsl:with-param>
					<xsl:with-param name="isDefault" select="vdef/value/text()='1'" />
				</xsl:call-template>
				<xsl:call-template name="selectOption">
					<xsl:with-param name="value" select="0" />
					<xsl:with-param name="label" >No</xsl:with-param>
					<xsl:with-param name="isDefault" select="vdef/value/text()='0'" />
				</xsl:call-template>				
			</xsl:if>
		</xhtml:select>
	</xsl:template>	

	<xsl:template match="parameter/*[substring(local-name(.),2,4)='list']//value" mode="controlChoice">
		<!-- Option sub-control for choices and booleans -->
		<xsl:call-template name="selectOption">
			<xsl:with-param name="value" select="text()" />
			<xsl:with-param name="label" >
				<xsl:choose>
					<xsl:when test="../label/text()">
						<xsl:value-of select="../label/text()"/>                
					</xsl:when>
					<xsl:otherwise></xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="isDefault" select="text()=ancestor-or-self::parameter/vdef/value/text()" />
		</xsl:call-template>
	</xsl:template>	

	<xsl:template name="selectOption">
		<xsl:param name="value" />
		<xsl:param name="label" />
		<xsl:param name="isDefault" />
		<xhtml:option value="{$value}">
			<xsl:if test="$isDefault">
				<!-- Default value -->
				<xsl:attribute name="selected" >selected</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="$label"/>                
		</xhtml:option>
	</xsl:template>		

</xsl:stylesheet>
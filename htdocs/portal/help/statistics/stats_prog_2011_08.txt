# -- 
# AccessLogInfos Report v0.3
# -- 
# Mobyle  Log file: ['/var/log/Mobyle/access_log', '/var/log/Mobyle/bkp/access_log']
# Period 2011/08/01-2011/08/31
FAF-Drugs2          :  709 jobs  28.66%   34 unique IPs
FAF-Drugs           :  636 jobs  25.71%   58 unique IPs
HCA                 :  315 jobs  12.73%  198 unique IPs
iSuperpose          :  174 jobs   7.03%    4 unique IPs
PEP-FOLD            :  172 jobs   6.95%   81 unique IPs
ASA                 :  156 jobs   6.31%    5 unique IPs
OpenBabel           :   75 jobs   3.03%   13 unique IPs
PASS                :   46 jobs   1.86%   32 unique IPs
wwLig-CSRre         :   28 jobs   1.13%   17 unique IPs
Frog2               :   18 jobs   0.73%    2 unique IPs
Bank-Formatter      :   12 jobs   0.49%    9 unique IPs
psipred             :   12 jobs   0.49%    8 unique IPs
dnadist             :   11 jobs   0.44%    2 unique IPs
PCE-pKa             :   10 jobs   0.40%    5 unique IPs
SSpro               :   10 jobs   0.40%    8 unique IPs
DeSalt              :    8 jobs   0.32%    2 unique IPs
HMDPocket           :    7 jobs   0.28%    2 unique IPs
LogP                :    7 jobs   0.28%    3 unique IPs
fpocket             :    5 jobs   0.20%    4 unique IPs
fpocket2            :    5 jobs   0.20%    4 unique IPs
antigenic           :    4 jobs   0.16%    1 unique IPs
CysState            :    3 jobs   0.12%    2 unique IPs
Dihedrals           :    3 jobs   0.12%    1 unique IPs
Filter-Editor       :    3 jobs   0.12%    2 unique IPs
coudes              :    3 jobs   0.12%    3 unique IPs
p-sea               :    3 jobs   0.12%    2 unique IPs
showalign           :    3 jobs   0.12%    1 unique IPs
Frog                :    2 jobs   0.08%    2 unique IPs
Modeller            :    2 jobs   0.08%    2 unique IPs
cai                 :    2 jobs   0.08%    1 unique IPs
consense            :    2 jobs   0.08%    2 unique IPs
extractTurn         :    2 jobs   0.08%    2 unique IPs
needle              :    2 jobs   0.08%    2 unique IPs
newcpgseek          :    2 jobs   0.08%    2 unique IPs
squizz_checker      :    2 jobs   0.08%    1 unique IPs
AMMOS               :    1 jobs   0.04%    1 unique IPs
DG-AMMOS            :    1 jobs   0.04%    1 unique IPs
H-bonds             :    1 jobs   0.04%    1 unique IPs
HMMEncode           :    1 jobs   0.04%    1 unique IPs
JME                 :    1 jobs   0.04%    1 unique IPs
PCE-pot             :    1 jobs   0.04%    1 unique IPs
PoPMuSic            :    1 jobs   0.04%    1 unique IPs
ProbCons            :    1 jobs   0.04%    1 unique IPs
QMean               :    1 jobs   0.04%    1 unique IPs
cpgplot             :    1 jobs   0.04%    1 unique IPs
cpgreport           :    1 jobs   0.04%    1 unique IPs
geecee              :    1 jobs   0.04%    1 unique IPs
helixturnhelix      :    1 jobs   0.04%    1 unique IPs
hmoment             :    1 jobs   0.04%    1 unique IPs
jmol_example        :    1 jobs   0.04%    1 unique IPs
newcpgreport        :    1 jobs   0.04%    1 unique IPs
plotSC              :    1 jobs   0.04%    1 unique IPs
stretcher           :    1 jobs   0.04%    1 unique IPs
stride              :    1 jobs   0.04%    1 unique IPs
tcode               :    1 jobs   0.04%    1 unique IPs
# --------------------------------------------------------------------
Total               : 2474 jobs (537 unique IPs)

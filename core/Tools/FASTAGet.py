#!/usr/bin/env python
# FASTAGet.py
#
# A simple loader parser for FASTA files
#
# Author: P. Tuffery, RPBS, INSERM UMR-S 726, 2008
# Adapted from Fasta.py (2001-2008) by P. Tuffery, RPBS, INSERM, France
#
# No warranty of any kind is provided
#
"""
Classe FASTAGet pour chercher une entree Fasta dans un multifasta

Try locally only (depends on : GDFLTPDBDIR and GDFLTSCOPDIR)
"""

import sys

# sys.path.append("/data/PyTools/Classes")

BANKSDIR = {
	 "pdbaa" : "/scratch/banks/pdb/derived_data/pdb_seqres.txt",	
}

from Fasta.Fasta import *
     
if __name__ == "__main__":
	
	try:
		bank = sys.argv[1].split(":")[0]
		id   = sys.argv[1].split(":")[1]	
	except:
		bank = "pdbaa"
		id   = "1tim_A"
		sys.stderr.write("Sorry: incorrect request. Argument format must match %s:%s\n" % (bank,id))
		sys.exit(0)
	try:
		x = fasta(BANKSDIR[bank], verbose = 0)
	except:
		sys.stderr.write("Sorry: no such bank %s\n" % (bank))
		sys.exit(0)
	try:
		if id not in x.ids():
			sys.stderr.write("Sorry: %s, no such Id in %s.\n" % (id, bank))	
			idList = " ".join(x.ids())
			idcount = idList.count(id[:4].lower())
			aId = None
			if idcount:
				idex = idList.index(id[:4].lower())
				aId = idList[idex:].split()[0]
			if aId != None:
				sys.stderr.write("You may try e.g. %s.\n" % (aId))
			sys.exit(0)
	except:
		sys.exit(0)
		pass
	# sys.stderr.write("Sorry: %s, %s no match found\n" % (id, bank))	
	# sys.exit(0)
		
	x.out(Ids = [id])


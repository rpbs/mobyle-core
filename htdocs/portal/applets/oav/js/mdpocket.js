
function js_hotspot_select(obj) {
    var btext=document.getElementById("bcutoff").value;
    if(btext != "") {
        if(isNaN(btext)) {
            alert("Invalid value for bfactor cutoff!") ;        
        }
        else {
            if(obj.checked) {
                av_execute("define hotspot byresidue (aminoacid and b > "+btext+"); select group hotspot ;") ;
            }
            else {
                av_execute("exclude group hotspot ;") ;
            }
        }
    }
}

function js_hotspot_spheres(obj) {
    var btext=document.getElementById("bcutoff").value;

    if(btext != "") {
        if(isNaN(btext)) {
            alert("Invalid value for bfactor cutoff!") ;        
        }
        else {
            if(obj.checked) {
                av_execute("define sphereHotspot byresidue (aminoacid and b > "+btext+"); display spheres group sphereHotspot ;") ;
            }
            else {
                av_execute("display spheres off group sphereHotspot ;") ;
            }
        }
    }
}

function js_hotspot_surface(obj) {
    var btext=document.getElementById("bcutoff").value;
    if(btext != "") {
        if(isNaN(btext)) {
            alert("Invalid value for bfactor cutoff!") ;        
        }
        else {
            if(obj.checked) {
                av_execute("define hotspot (byresidue aminoacid and b > "+btext+"); surface -solid true hotsurf red group hotspot ;") ;
            }
            else {
                av_execute("object remove hotsurf ;") ;
            }
        }
    }
}

function resetHP(){
    document.getElementById("HPSel").checked = 0;
    av_execute("exclude group hotspot ;") ;

    document.getElementById("HPSph").checked = 0;
    av_execute("display spheres off group sphereHotspot ;") ;
      
    document.getElementById("HPSrf").checked = 0;
    av_execute("object remove hotsurf ;") ;
}

function updateHP(){
    var btext=document.getElementById("bcutoff").value;
    if( document.getElementById("HPSel").checked ) {
	av_execute("exclude group hotspot ;") ;
	av_execute("define hotspot byresidue (aminoacid and b > "+btext+"); select group hotspot ;") ;
    }

    if( document.getElementById("HPSph").checked ){
	av_execute("display spheres off group sphereHotspot ;") ;
	av_execute("define sphereHotspot byresidue (aminoacid and b > "+btext+"); display spheres group sphereHotspot ;") ;
    }
      
    if( document.getElementById("HPSrf").checked ){
	av_execute("object remove hotsurf ;") ;
	av_execute("define hotspot (byresidue aminoacid and b > "+btext+"); surface -solid true hotsurf red group hotspot ;") ;
    }
}

import sys
import os.path


from logging  import getLogger
p_log = getLogger('Mobyle.Policy')



def queue( queueName ):
    """
    @return: the name of the queue to be used to execute the job
    @rtype: string
    """
    return queueName



#if you want to defined a local policy to check user
# you must put you code here 
def emailCheck( **args ):
     """
     check if the email according to the local rules.
     @return:
      - Mobyle.Net.EmailAddress.VALID    if the email is valid
      - Mobyle.Net.EmailAddress.INVALID  if the email is rejected
      - Mobyle.Net.EmailAddress.CONTINUE to continue futher the email validation process
     """
     import Mobyle.Net

     return Mobyle.Net.EmailAddress.CONTINUE
 


def authenticate( login , passwd ):
    """
    Mobyle administartor can put authentification code here. this function must return either 
     - Mobyle.AuthenticatedSession.AuthenticatedSession.CONTINUE: 
                the method does not autentified this login/passwd. The fall back method must be applied.
     - Mobyle.AuthenticatedSession.AuthenticatedSession.VALID:    
               the login/password has been authenticated.
     - Mobyle.AuthenticatedSession.AuthenticatedSession.REJECT:    
              the login/password is not allow to continue.
    """
    import Mobyle.AuthenticatedSession
    
    return Mobyle.AuthenticatedSession.AuthenticatedSession.CONTINUE



def allow_to_be_executed( job ):
    """
    check if the job is allowed to be executed
    if a job is not allowed must raise a UserError
    @param job: the job to check before to submit it to Execution
    @type job: L{Job} instance
    @raise UserValueError: if the job is not allowed to be executed
    """
    #place here the code you want to be executed to allow a job to be executed.
    #this is the last control be fore DRM submission
    #if you want to limit simultaneous job according some criteria as IP or user email, ...
    #this this the right place to put your code
    return True

